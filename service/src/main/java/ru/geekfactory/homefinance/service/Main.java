package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.service.services.AccountService;
import ru.geekfactory.homefinance.service.services.CategoryTransactionService;
import ru.geekfactory.homefinance.service.services.TransactionService;

import java.util.Collection;

public class Main {
    public static void main(String[] args) {
//        testCategoryService();
//        testAccountService();
        TransactionService transactionService = new TransactionService();
        Collection<TransactionModel> transactionModels;
        transactionModels = transactionService.findAll();

        for (TransactionModel setElement: transactionModels) {
            System.out.println(setElement.toString());
        }
    }

    public static void testCategoryService() {
        CategoryTransactionService categoryTransactionService = new CategoryTransactionService();
        System.out.println(categoryTransactionService.findById(1l));
        for (CategoryTransactionModel setElement: categoryTransactionService.findAllSubCategories(1l)) {
            System.out.println(setElement);
        }
    }

    public static void testAccountService() {
        AccountService accountService = new AccountService();
        System.out.println(accountService.findById(3l));
        for (AccountModel setElement: accountService.findAll()) {
            System.out.println(setElement);
        }
    }
}
