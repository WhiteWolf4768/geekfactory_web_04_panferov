package ru.geekfactory.homefinance.service.services;

import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.dao.repository.BankRepository;

import java.util.Collection;
import java.util.Optional;

public class BankService implements Service<BankModel> {
    private BankRepository bankRepository;

    public BankService() {
        this.bankRepository = new BankRepository();
    }

    @Override
    public Optional<BankModel> findById(Long id) {
        return bankRepository.findById(id);
    }

    @Override
    public Collection<BankModel> findAll() {
        return bankRepository.findAll();
    }

    @Override
    public BankModel insert(BankModel model) {
        return bankRepository.insert(model);
    }

    @Override
    public void update(BankModel model) {
        bankRepository.update(model);
    }

    @Override
    public void delete(Long id) {
        bankRepository.delete(id);
    }
}
