package ru.geekfactory.homefinance.service.services;

import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.repository.CategoryTransactionRepository;

import java.util.Collection;
import java.util.Optional;

public class CategoryTransactionService implements Service<CategoryTransactionModel> {
    private CategoryTransactionRepository categoryTransactionRepository;

    public CategoryTransactionService() {
        this.categoryTransactionRepository = new CategoryTransactionRepository();
    }

    @Override
    public Optional<CategoryTransactionModel> findById(Long id) {
        return categoryTransactionRepository.findById(id);
    }

    @Override
    public Collection<CategoryTransactionModel> findAll() {
        return categoryTransactionRepository.findAll();
    }

    @Override
    public CategoryTransactionModel insert(CategoryTransactionModel model) {
        return categoryTransactionRepository.insert(model);
    }

    @Override
    public void update(CategoryTransactionModel model) {
        categoryTransactionRepository.update(model);
    }

    @Override
    public void delete(Long id) {
        categoryTransactionRepository.delete(id);
    }

    public Collection<CategoryTransactionModel> findAllSubCategories(Long id) {
        return categoryTransactionRepository.findAllSubCategories(id);
    }
}
