package ru.geekfactory.homefinance.service.services;

import java.util.Collection;
import java.util.Optional;

public interface Service<T> {
    Optional<T> findById(Long id);
    Collection<T> findAll();
    T insert(T model);
    void update(T model);
    void delete(Long id);
}
