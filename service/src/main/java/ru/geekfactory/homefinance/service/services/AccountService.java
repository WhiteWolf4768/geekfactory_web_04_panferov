package ru.geekfactory.homefinance.service.services;

import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.repository.AccountRepository;

import java.util.Collection;
import java.util.Optional;

public class AccountService implements Service<AccountModel> {
    private AccountRepository accountRepository;

    public AccountService() {
        this.accountRepository = new AccountRepository();
    }

    @Override
    public Optional<AccountModel> findById(Long id) {
        return accountRepository.findById(id);
    }

    @Override
    public Collection<AccountModel> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public AccountModel insert(AccountModel model) {
        return accountRepository.insert(model);
    }

    @Override
    public void update(AccountModel model) {
        accountRepository.update(model);
    }

    @Override
    public void delete(Long id) {
        accountRepository.delete(id);
    }
}
