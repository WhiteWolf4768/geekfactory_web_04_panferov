package ru.geekfactory.homefinance.service.services;

import ru.geekfactory.homefinance.dao.model.AccountTypeModel;
import ru.geekfactory.homefinance.dao.repository.AccountTypeRepository;

import java.util.Collection;
import java.util.Optional;

public class AccountTypeService implements Service<AccountTypeModel> {
    private AccountTypeRepository accountTypeRepository;

    public AccountTypeService() {
        this.accountTypeRepository = new AccountTypeRepository();
    }

    @Override
    public Optional<AccountTypeModel> findById(Long id) {
        return accountTypeRepository.findById(id);
    }

    @Override
    public Collection<AccountTypeModel> findAll() {
        return accountTypeRepository.findAll();
    }

    @Override
    public AccountTypeModel insert(AccountTypeModel model) {
        return accountTypeRepository.insert(model);
    }

    @Override
    public void update(AccountTypeModel model) {
        accountTypeRepository.update(model);
    }

    @Override
    public void delete(Long id) {
        accountTypeRepository.delete(id);
    }
}
