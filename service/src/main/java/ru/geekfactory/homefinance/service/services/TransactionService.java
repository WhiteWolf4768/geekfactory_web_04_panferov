package ru.geekfactory.homefinance.service.services;

import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.dao.repository.TransactionRepository;

import java.util.Collection;
import java.util.Optional;

public class TransactionService implements Service<TransactionModel> {
    private TransactionRepository transactionRepository;

    public TransactionService() {
        this.transactionRepository = new TransactionRepository();
    }

    @Override
    public Optional<TransactionModel> findById(Long id) {
        return transactionRepository.findById(id);
    }

    @Override
    public Collection<TransactionModel> findAll() {
        return transactionRepository.findAll();
    }

    @Override
    public TransactionModel insert(TransactionModel model) {
        return transactionRepository.insert(model);
    }

    @Override
    public void update(TransactionModel model) {
        transactionRepository.update(model);
    }

    @Override
    public void delete(Long id) {
        transactionRepository.delete(id);
    }
}
