package ru.geekfactory.homefinance.service.services;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;

import java.util.Collection;
import java.util.Optional;

public class CurrencyService implements Service<CurrencyModel> {
    private CurrencyRepository currencyRepository;

    public CurrencyService() {
        this.currencyRepository = new CurrencyRepository();
    }

    @Override
    public Optional<CurrencyModel> findById(Long id) {
        return currencyRepository.findById(id);
    }

    @Override
    public Collection<CurrencyModel> findAll() {
        return currencyRepository.findAll();
    }

    @Override
    public CurrencyModel insert(CurrencyModel model) {
        return currencyRepository.insert(model);
    }

    @Override
    public void update(CurrencyModel model) {
        currencyRepository.update(model);
    }

    @Override
    public void delete(Long id) {
        currencyRepository.delete(id);
    }
}
