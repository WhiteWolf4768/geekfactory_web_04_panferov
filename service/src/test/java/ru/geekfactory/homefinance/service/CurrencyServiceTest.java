package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;
import ru.geekfactory.homefinance.service.services.CurrencyService;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceTest {
    @InjectMocks
    private CurrencyService currencyService;
    
    @Mock
    private CurrencyRepository currencyRepositoryMock;

    @Test
    @DisplayName("CurrencyService find by id test")
    void findByIdTest() {
        CurrencyModel currencyModel = new CurrencyModel(1l, "Rubls", "RU", "R");
        when(currencyRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(currencyModel));

        Assertions.assertNotNull(currencyService);

        CurrencyModel actualCurrencyModel = currencyService.findById(1l).orElse(null);
        Assertions.assertEquals(currencyModel, actualCurrencyModel);

        verify(currencyRepositoryMock, times(1)).findById(anyLong());
    }

    @Test
    @DisplayName("CurrencyService find all test")
    void findAllTest() {
        CurrencyModel currencyModel = new CurrencyModel(1l, "Rubls", "RU", "R");
        CurrencyModel currencyModel1 = new CurrencyModel(2l, "Euro", "EU", "E");
        Collection<CurrencyModel> currencyModels = new HashSet<>();
        currencyModels.add(currencyModel);
        currencyModels.add(currencyModel1);

        when(currencyRepositoryMock.findAll()).thenReturn(currencyModels);

        Assertions.assertNotNull(currencyService);

        Collection<CurrencyModel> actualCurrencyModels = currencyService.findAll();
        Assertions.assertEquals(currencyModels, actualCurrencyModels);
        Assertions.assertEquals(2, actualCurrencyModels.size());

        verify(currencyRepositoryMock, times(1)).findAll();
    }

    @Test
    @DisplayName("CurrencyService insert test")
    void insertTest() {
        CurrencyModel currencyModel = new CurrencyModel(1l, "Rubls", "RU", "R");
        CurrencyModel insertCurrencyModel = new CurrencyModel("Rubls", "RU", "R");

        when(currencyRepositoryMock.insert(any(CurrencyModel.class))).thenReturn(currencyModel);

        Assertions.assertNotNull(currencyService);

        CurrencyModel actualCurrencyModel = currencyService.insert(insertCurrencyModel);
        Assertions.assertEquals(currencyModel, actualCurrencyModel);

        verify(currencyRepositoryMock, times(1)).insert(any(CurrencyModel.class));
    }

    @Test
    @DisplayName("CurrencyService update test")
    void updateTest() {
        CurrencyModel currencyModel = new CurrencyModel(1l, "Rubls", "RU", "R");

        doNothing().when(currencyRepositoryMock).update(any(CurrencyModel.class));

        Assertions.assertNotNull(currencyService);

        currencyService.update(currencyModel);

        verify(currencyRepositoryMock, times(1)).update(any(CurrencyModel.class));
    }

    @Test
    @DisplayName("CurrencyService delete test")
    void deleteTest() {
        when(currencyRepositoryMock.delete(anyLong())).thenReturn(true);

        Assertions.assertNotNull(currencyService);

        currencyService.delete(111l);

        verify(currencyRepositoryMock, times(1)).delete(anyLong());
    }
}
