package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.AccountTypeModel;
import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.AccountRepository;
import ru.geekfactory.homefinance.service.services.AccountService;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    @InjectMocks
    private AccountService accountService;
    
    @Mock
    private AccountRepository accountRepositoryMock;

    @Test
    @DisplayName("AccountService find by id test")
    void findByIdTest() {
        AccountModel accountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Euro", "EU", "E"),
                new AccountTypeModel(1l, "Cash"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));
        when(accountRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(accountModel));

        Assertions.assertNotNull(accountService);

        AccountModel actualAccountModel = accountService.findById(1l).orElse(null);
        Assertions.assertEquals(accountModel, actualAccountModel);

        verify(accountRepositoryMock, times(1)).findById(anyLong());
    }

    @Test
    @DisplayName("AccountService find all test")
    void findAllTest() {
        AccountModel accountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Euro", "EU", "E"),
                new AccountTypeModel(1l, "Cash"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        AccountModel accountModel1 = new AccountModel(2l, "Acc2",
                new CurrencyModel(1l, "Euro", "EU", "E"),
                new AccountTypeModel(1l, "CARD"),
                new BankModel(1l, "ALPHA"),
                true, new BigDecimal("650.00"));

        Collection<AccountModel> accountModels = new HashSet<>();
        accountModels.add(accountModel);
        accountModels.add(accountModel1);

        when(accountRepositoryMock.findAll()).thenReturn(accountModels);

        Assertions.assertNotNull(accountService);

        Collection<AccountModel> actualAccountModels = accountService.findAll();
        Assertions.assertEquals(accountModels, actualAccountModels);
        Assertions.assertEquals(2, actualAccountModels.size());

        verify(accountRepositoryMock, times(1)).findAll();
    }

    @Test
    @DisplayName("AccountService insert test")
    void insertTest() {
        AccountModel accountModel = new AccountModel("Acc1",
                new CurrencyModel(1l, "Euro", "EU", "E"),
                new AccountTypeModel(1l, "Cash"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        AccountModel insertAccountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Euro", "EU", "E"),
                new AccountTypeModel(1l, "Cash"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        when(accountRepositoryMock.insert(any(AccountModel.class))).thenReturn(accountModel);

        Assertions.assertNotNull(accountService);

        AccountModel actualAccountModel = accountService.insert(insertAccountModel);
        Assertions.assertEquals(accountModel, actualAccountModel);

        verify(accountRepositoryMock, times(1)).insert(any(AccountModel.class));
    }

    @Test
    @DisplayName("AccountService update test")
    void updateTest() {
        AccountModel accountModel = new AccountModel(1l, "ACCOUNT",
                new CurrencyModel(1l, "Euro", "EU", "E"),
                new AccountTypeModel(1l, "Cash"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        doNothing().when(accountRepositoryMock).update(any(AccountModel.class));

        Assertions.assertNotNull(accountService);

        accountService.update(accountModel);

        verify(accountRepositoryMock, times(1)).update(any(AccountModel.class));
    }

    @Test
    @DisplayName("AccountService delete test")
    void deleteTest() {
        when(accountRepositoryMock.delete(anyLong())).thenReturn(true);

        Assertions.assertNotNull(accountService);

        accountService.delete(111l);

        verify(accountRepositoryMock, times(1)).delete(anyLong());
    }
}
