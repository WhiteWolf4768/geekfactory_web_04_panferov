package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.repository.CategoryTransactionRepository;
import ru.geekfactory.homefinance.service.services.CategoryTransactionService;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CategoryTransactionServiceTest {
    @InjectMocks
    private CategoryTransactionService categoryTransactionService;
    
    @Mock
    private CategoryTransactionRepository categoryTransactionRepositoryMock;

    @Test
    @DisplayName("CategoryTransactionService find by id test")
    void findByIdTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "Cat1", null);
        when(categoryTransactionRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(categoryTransactionModel));

        Assertions.assertNotNull(categoryTransactionService);

        CategoryTransactionModel actualCategoryTransactionModel = categoryTransactionService.findById(1l).orElse(null);
        Assertions.assertEquals(categoryTransactionModel, actualCategoryTransactionModel);

        verify(categoryTransactionRepositoryMock, times(1)).findById(anyLong());
    }

    @Test
    @DisplayName("CategoryTransactionService find all test")
    void findAllTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "Cat1", null);
        CategoryTransactionModel categoryTransactionModel1 = new CategoryTransactionModel(2l, "Cat2", categoryTransactionModel);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel);
        categoryTransactionModels.add(categoryTransactionModel1);

        when(categoryTransactionRepositoryMock.findAll()).thenReturn(categoryTransactionModels);

        Assertions.assertNotNull(categoryTransactionService);

        Collection<CategoryTransactionModel> actualCategoryTransactionModels = categoryTransactionService.findAll();
        Assertions.assertEquals(categoryTransactionModels, actualCategoryTransactionModels);
        Assertions.assertEquals(2, actualCategoryTransactionModels.size());

        verify(categoryTransactionRepositoryMock, times(1)).findAll();
    }

    @Test
    @DisplayName("CategoryTransactionService find all subcategories test")
    void findAllSubcategoriesTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "Cat1", null);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel);

        when(categoryTransactionRepositoryMock.findAllSubCategories(anyLong())).thenReturn(categoryTransactionModels);

        Assertions.assertNotNull(categoryTransactionService);

        Collection<CategoryTransactionModel> actualCategoryTransactionModels = categoryTransactionService.findAllSubCategories(1l);
        Assertions.assertEquals(categoryTransactionModels, actualCategoryTransactionModels);
        Assertions.assertEquals(1, actualCategoryTransactionModels.size());

        verify(categoryTransactionRepositoryMock, times(1)).findAllSubCategories(anyLong());
    }

    @Test
    @DisplayName("CategoryTransactionService insert test")
    void insertTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "Cat1", null);
        CategoryTransactionModel insertCategoryTransactionModel = new CategoryTransactionModel("Cat1", null);

        when(categoryTransactionRepositoryMock.insert(any(CategoryTransactionModel.class))).thenReturn(categoryTransactionModel);

        Assertions.assertNotNull(categoryTransactionService);

        CategoryTransactionModel actualCategoryTransactionModel = categoryTransactionService.insert(insertCategoryTransactionModel);
        Assertions.assertEquals(categoryTransactionModel, actualCategoryTransactionModel);

        verify(categoryTransactionRepositoryMock, times(1)).insert(any(CategoryTransactionModel.class));
    }

    @Test
    @DisplayName("CategoryTransactionService update test")
    void updateTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "Cat1", null);

        doNothing().when(categoryTransactionRepositoryMock).update(any(CategoryTransactionModel.class));

        Assertions.assertNotNull(categoryTransactionService);

        categoryTransactionService.update(categoryTransactionModel);

        verify(categoryTransactionRepositoryMock, times(1)).update(any(CategoryTransactionModel.class));
    }

    @Test
    @DisplayName("CategoryTransactionService delete test")
    void deleteTest() {
        when(categoryTransactionRepositoryMock.delete(anyLong())).thenReturn(true);

        Assertions.assertNotNull(categoryTransactionService);

        categoryTransactionService.delete(111l);

        verify(categoryTransactionRepositoryMock, times(1)).delete(anyLong());
    }
}
