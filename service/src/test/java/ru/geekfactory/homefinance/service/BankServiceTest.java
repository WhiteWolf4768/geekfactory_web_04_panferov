package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.dao.repository.BankRepository;
import ru.geekfactory.homefinance.service.services.BankService;

@ExtendWith(MockitoExtension.class)
public class BankServiceTest {

    @InjectMocks
    private BankService bankService;

    @Mock
    private BankRepository bankRepositoryMock;

    @Test
    @DisplayName("BankService find by id test")
    void findByIdTest() {
        BankModel bankModel = new BankModel(1l, "sberbank");
        when(bankRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(bankModel));

        Assertions.assertNotNull(bankService);

        BankModel actualBankModel = bankService.findById(1l).orElse(null);
        Assertions.assertEquals(bankModel, actualBankModel);

        verify(bankRepositoryMock, times(1)).findById(anyLong());
    }

    @Test
    @DisplayName("BankService find all test")
    void findAllTest() {
        BankModel bankModel = new BankModel(1l, "sberbank");
        BankModel bankModel1 = new BankModel(2l, "alpha");
        Collection<BankModel> bankModels = new HashSet<>();
        bankModels.add(bankModel);
        bankModels.add(bankModel1);

        when(bankRepositoryMock.findAll()).thenReturn(bankModels);

        Assertions.assertNotNull(bankService);

        Collection<BankModel> actualBankModels = bankService.findAll();
        Assertions.assertEquals(bankModels, actualBankModels);
        Assertions.assertEquals(2, actualBankModels.size());

        verify(bankRepositoryMock, times(1)).findAll();
    }

    @Test
    @DisplayName("BankService insert test")
    void insertTest() {
        BankModel bankModel = new BankModel(1l, "sberbank");
        BankModel insertBankModel = new BankModel("sberbank");

        when(bankRepositoryMock.insert(any(BankModel.class))).thenReturn(bankModel);

        Assertions.assertNotNull(bankService);

        BankModel actualBankModel = bankService.insert(insertBankModel);
        Assertions.assertEquals(bankModel, actualBankModel);

        verify(bankRepositoryMock, times(1)).insert(any(BankModel.class));
    }

    @Test
    @DisplayName("BankService update test")
    void updateTest() {
        BankModel bankModel = new BankModel(1l,"SBERBANK");

        doNothing().when(bankRepositoryMock).update(any(BankModel.class));

        Assertions.assertNotNull(bankService);

        bankService.update(bankModel);

        verify(bankRepositoryMock, times(1)).update(any(BankModel.class));
    }

    @Test
    @DisplayName("BankService delete test")
    void deleteTest() {
        when(bankRepositoryMock.delete(anyLong())).thenReturn(true);

        Assertions.assertNotNull(bankService);

        bankService.delete(111l);

        verify(bankRepositoryMock, times(1)).delete(anyLong());
    }
}
