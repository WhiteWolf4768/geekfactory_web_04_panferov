package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import ru.geekfactory.homefinance.dao.model.AccountTypeModel;
import ru.geekfactory.homefinance.dao.repository.AccountTypeRepository;
import ru.geekfactory.homefinance.service.services.AccountTypeService;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class AccountTypeServiceTest {
    @InjectMocks
    private AccountTypeService accountTypeService;
    
    @Mock
    private AccountTypeRepository accountTypeRepositoryMock;

    @Test
    @DisplayName("AccountTypeService find by id test")
    void findByIdTest() {
        AccountTypeModel accountTypeModel = new AccountTypeModel(1l, "Cash");
        when(accountTypeRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(accountTypeModel));

        Assertions.assertNotNull(accountTypeService);

        AccountTypeModel actualAccountTypeModel = accountTypeService.findById(1l).orElse(null);
        Assertions.assertEquals(accountTypeModel, actualAccountTypeModel);

        verify(accountTypeRepositoryMock, times(1)).findById(anyLong());
    }

    @Test
    @DisplayName("AccountTypeService find all test")
    void findAllTest() {
        AccountTypeModel accountTypeModel = new AccountTypeModel(1l, "Cash");
        AccountTypeModel accountTypeModel1 = new AccountTypeModel(2l, "Card");
        Collection<AccountTypeModel> accountTypeModels = new HashSet<>();
        accountTypeModels.add(accountTypeModel);
        accountTypeModels.add(accountTypeModel1);

        when(accountTypeRepositoryMock.findAll()).thenReturn(accountTypeModels);

        Assertions.assertNotNull(accountTypeService);

        Collection<AccountTypeModel> actualAccountTypeModels = accountTypeService.findAll();
        Assertions.assertEquals(accountTypeModels, actualAccountTypeModels);
        Assertions.assertEquals(2, actualAccountTypeModels.size());

        verify(accountTypeRepositoryMock, times(1)).findAll();
    }

    @Test
    @DisplayName("AccountTypeService insert test")
    void insertTest() {
        AccountTypeModel accountTypeModel = new AccountTypeModel(1l, "Cash");
        AccountTypeModel insertAccountTypeModel = new AccountTypeModel("Cash");

        when(accountTypeRepositoryMock.insert(any(AccountTypeModel.class))).thenReturn(accountTypeModel);

        Assertions.assertNotNull(accountTypeService);

        AccountTypeModel actualAccountTypeModel = accountTypeService.insert(insertAccountTypeModel);
        Assertions.assertEquals(accountTypeModel, actualAccountTypeModel);

        verify(accountTypeRepositoryMock, times(1)).insert(any(AccountTypeModel.class));
    }

    @Test
    @DisplayName("AccountTypeService update test")
    void updateTest() {
        AccountTypeModel accountTypeModel = new AccountTypeModel(1l,"CASH");

        doNothing().when(accountTypeRepositoryMock).update(any(AccountTypeModel.class));

        Assertions.assertNotNull(accountTypeService);

        accountTypeService.update(accountTypeModel);

        verify(accountTypeRepositoryMock, times(1)).update(any(AccountTypeModel.class));
    }

    @Test
    @DisplayName("AccountTypeService delete test")
    void deleteTest() {
        when(accountTypeRepositoryMock.delete(anyLong())).thenReturn(true);

        Assertions.assertNotNull(accountTypeService);

        accountTypeService.delete(111l);

        verify(accountTypeRepositoryMock, times(1)).delete(anyLong());
    }
}
