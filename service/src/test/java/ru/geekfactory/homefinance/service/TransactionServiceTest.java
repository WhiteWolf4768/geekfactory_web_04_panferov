package ru.geekfactory.homefinance.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import ru.geekfactory.homefinance.dao.model.*;
import ru.geekfactory.homefinance.dao.repository.TransactionRepository;
import ru.geekfactory.homefinance.service.services.TransactionService;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {
    @InjectMocks
    private TransactionService transactionService;
    
    @Mock
    private TransactionRepository transactionRepositoryMock;

    private static TransactionModel getTestModel() {
        AccountModel accountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Rub", "RU", "R"),
                new AccountTypeModel(1l, "Card"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "cat1", null);
        CategoryTransactionModel categoryTransactionModel1 = new CategoryTransactionModel(2l, "cat2", null);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel);
        categoryTransactionModels.add(categoryTransactionModel1);

        TransactionModel transactionModel = new TransactionModel(1l, new BigDecimal("200.00"), LocalDateTime.of(2008, 10, 23, 10, 37,22), accountModel, categoryTransactionModels);
        return transactionModel;
    }

    @Test
    @DisplayName("TransactionService find by id test")
    void findByIdTest() {
        TransactionModel transactionModel = getTestModel();
        when(transactionRepositoryMock.findById(anyLong())).thenReturn(Optional.ofNullable(transactionModel));

        Assertions.assertNotNull(transactionService);

        TransactionModel actualTransactionModel = transactionService.findById(1l).orElse(null);
        Assertions.assertEquals(transactionModel, actualTransactionModel);

        verify(transactionRepositoryMock, times(1)).findById(anyLong());
    }

    @Test
    @DisplayName("TransactionService find all test")
    void findAllTest() {
        TransactionModel transactionModel = getTestModel();
        Collection<TransactionModel> transactionModels = new HashSet<>();
        transactionModels.add(transactionModel);

        when(transactionRepositoryMock.findAll()).thenReturn(transactionModels);

        Assertions.assertNotNull(transactionService);

        Collection<TransactionModel> actualTransactionModels = transactionService.findAll();
        Assertions.assertEquals(transactionModels, actualTransactionModels);
        Assertions.assertEquals(1, actualTransactionModels.size());

        verify(transactionRepositoryMock, times(1)).findAll();
    }

    @Test
    @DisplayName("TransactionService insert test")
    void insertTest() {
        TransactionModel transactionModel = getTestModel();
        TransactionModel insertTransactionModel = getTestModel();

        when(transactionRepositoryMock.insert(any(TransactionModel.class))).thenReturn(transactionModel);

        Assertions.assertNotNull(transactionService);

        TransactionModel actualTransactionModel = transactionService.insert(insertTransactionModel);
        Assertions.assertEquals(transactionModel, actualTransactionModel);

        verify(transactionRepositoryMock, times(1)).insert(any(TransactionModel.class));
    }

    @Test
    @DisplayName("TransactionService update test")
    void updateTest() {
        AccountModel accountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Euro", "EU", "E"),
                new AccountTypeModel(1l, "Cash"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "Cat1", null);
        CategoryTransactionModel categoryTransactionModel1 = new CategoryTransactionModel(2l, "Cat2", categoryTransactionModel);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel);
        categoryTransactionModels.add(categoryTransactionModel1);

        TransactionModel transactionModel = new TransactionModel(1l, new BigDecimal("3000.00"), LocalDateTime.now(), accountModel, categoryTransactionModels);


        doNothing().when(transactionRepositoryMock).update(any(TransactionModel.class));

        Assertions.assertNotNull(transactionService);

        transactionService.update(transactionModel);

        verify(transactionRepositoryMock, times(1)).update(any(TransactionModel.class));
    }

    @Test
    @DisplayName("TransactionService delete test")
    void deleteTest() {
        when(transactionRepositoryMock.delete(anyLong())).thenReturn(true);

        Assertions.assertNotNull(transactionService);

        transactionService.delete(111l);

        verify(transactionRepositoryMock, times(1)).delete(anyLong());
    }
}
