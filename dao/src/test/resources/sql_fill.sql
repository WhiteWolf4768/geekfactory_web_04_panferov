INSERT INTO categoryTbl (name, parentCategoryId) VALUES ('cat1', null);
INSERT INTO categoryTbl (name, parentCategoryId) VALUES ('cat2', null);
INSERT INTO categoryTbl (name, parentCategoryId) VALUES ('subCat11', 1);
INSERT INTO categoryTbl (name, parentCategoryId) VALUES ('SUBsubCat111', 3);

INSERT INTO currencyTbl (name, code, symbol) VALUES ('Rub', 'RU', 'R');
INSERT INTO currencyTbl (name, code, symbol) VALUES ('Euro', 'EU', 'E');

INSERT INTO bankTbl (name) VALUES ('sberbank');
INSERT INTO bankTbl (name) VALUES ('alpha');
INSERT INTO bankTbl (name) VALUES ('nya');

INSERT INTO accountTypeTbl (name) VALUES ('Card');
INSERT INTO accountTypeTbl (name) VALUES ('Cash');
INSERT INTO accountTypeTbl (name) VALUES ('Debit');

INSERT INTO accountTbl (name, isActive, amount, currencyId, accountTypeId, bankId)
VALUES ('Acc1', true, 3000, 1, 1, 1);

INSERT INTO transactionTbl (amount, dateTime, accountId) VALUES (200, '2008-10-23 10:37:22', 1);

INSERT INTO transactionCategoryTbl (transactionId, categoryId) VALUES (1, 1);
INSERT INTO transactionCategoryTbl (transactionId, categoryId) VALUES (1, 2);
