package ru.geekfactory.homefinance.dao.repository;

import lombok.Getter;
import org.h2.tools.RunScript;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;

import java.io.*;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

@Getter
public class SQLquery {

//    private String getQuery() {
//        try {
//            String sql = readFile(getClass().getResource("/init.db").getPath(), Charset.defaultCharset());
//        } catch (IOException e) {
//            throw new HomeFinanceDaoException("file init.db not found", e);
//        }
//    }

    /*public void createDatabase(ConnectionSupplier connectionSupplier) {
        try (Connection connection = connectionSupplier.getConnection()) {
            File script = new File(getClass().getResource("/sql_init.sql").getFile());
            RunScript.execute(connection, new FileReader(script));
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("Prepare error", e);
        } catch (FileNotFoundException e) {
            throw new HomeFinanceDaoException("SQL init script not found", e);
        }
    }

    public void fillDatabase(ConnectionSupplier connectionSupplier) {
        try (Connection connection = connectionSupplier.getConnection()) {
            File script = new File(getClass().getResource("/sql_fill.sql").getFile());
            RunScript.execute(connection, new FileReader(script));
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("Prepare error", e);
        } catch (FileNotFoundException e) {
            throw new HomeFinanceDaoException("SQL fill script not found", e);
        }
    }*/

    public void createDatabase(ConnectionSupplier connectionSupplier) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getQuery())) {
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("Prepare error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("Prepare error", e);
        }
    }

    public void fillDatabase(ConnectionSupplier connectionSupplier) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getFillQuery())) {
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("Fill error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("Fill error", e);
        }
    }

    public static String getQuery() {
        return "DROP TABLE IF EXISTS transactionCategoryTbl;\n" +
                "DROP TABLE IF EXISTS transactionTbl;\n" +
                "DROP TABLE IF EXISTS categoryTbl;\n" +
                "DROP TABLE IF EXISTS accountTypeTbl;\n" +
                "DROP TABLE IF EXISTS currencyTbl;\n" +
                "DROP TABLE IF EXISTS bankTbl;\n" +
                "DROP TABLE IF EXISTS accountTbl;\n" +
                "\n" +
                "create table currencyTbl (\n" +
                "    id int auto_increment primary key not null,\n" +
                "    name varchar(30) unique not null,\n" +
                "    code varchar(30) not null,\n" +
                "    symbol varchar(30) not null\n" +
                ");\n" +
                "\n" +
                "create table categoryTbl (\n" +
                "    id int auto_increment primary key not null,\n" +
                "    name varchar(30) unique not null,\n" +
                "\n" +
                "    parentCategoryId int,\n" +
                "    foreign key (parentCategoryId) references categoryTbl (id)\n" +
                ");\n" +
                "\n" +
                "create table bankTbl (\n" +
                "    id int auto_increment primary key not null,\n" +
                "    name varchar(30) unique not null\n" +
                ");\n" +
                "\n" +
                "create table accountTypeTbl (\n" +
                "    id int auto_increment primary key not null,\n" +
                "    name varchar(30) unique not null\n" +
                ");\n" +
                "\n" +
                "create table accountTbl (\n" +
                "    id int auto_increment primary key not null,\n" +
                "    name varchar(30) unique not null,\n" +
                "    isActive boolean not null,\n" +
                "    amount decimal(15, 2) not null,\n" +
                "\n" +
                "    currencyId int,\n" +
                "    foreign key (currencyId) references currencyTbl (id),\n" +
                "\n" +
                "    accountTypeId int,\n" +
                "    foreign key (accountTypeId) references accountTypeTbl (id),\n" +
                "\n" +
                "    bankId int,\n" +
                "    foreign key (bankId) references bankTbl (id)\n" +
                ");\n" +
                "\n" +
                "create table transactionTbl (\n" +
                "    id int auto_increment primary key not null,\n" +
                "    amount decimal(15, 2) not null,\n" +
                "    dateTime datetime not null,\n" +
                "\n" +
                "    accountId int,\n" +
                "    foreign key (accountId) references accountTbl (id)\n" +
                ");\n" +
                "\n" +
                "create table transactionCategoryTbl(\n" +
                "    id int auto_increment primary key not null,\n" +
                "    transactionId int not null,\n" +
                "    foreign key (transactionId) references transactionTbl (id),\n" +
                "    categoryId int not null,\n" +
                "    foreign key (categoryId) references categoryTbl (id)\n" +
                ");";
    }

    public static String getFillQuery() {
        return  "INSERT INTO categoryTbl (name, parentCategoryId) VALUES ('cat1', null);\n" +
                "INSERT INTO categoryTbl (name, parentCategoryId) VALUES ('cat2', null);\n" +
                "INSERT INTO categoryTbl (name, parentCategoryId) VALUES ('subCat11', 1);\n" +
                "INSERT INTO categoryTbl (name, parentCategoryId) VALUES ('SUBsubCat111', 3);\n" +
                "\n" +
                "INSERT INTO currencyTbl (name, code, symbol) VALUES ('Rub', 'RU', 'R');\n" +
                "INSERT INTO currencyTbl (name, code, symbol) VALUES ('Euro', 'EU', 'E');\n" +
                "\n" +
                "INSERT INTO bankTbl (name) VALUES ('sberbank');\n" +
                "INSERT INTO bankTbl (name) VALUES ('alpha');\n" +
                "INSERT INTO bankTbl (name) VALUES ('nya');\n" +
                "\n" +
                "INSERT INTO accountTypeTbl (name) VALUES ('Card');\n" +
                "INSERT INTO accountTypeTbl (name) VALUES ('Cash');\n" +
                "INSERT INTO accountTypeTbl (name) VALUES ('Debit');\n" +
                "\n" +
                "INSERT INTO accountTbl (name, isActive, amount, currencyId, accountTypeId, bankId)\n" +
                "VALUES ('Acc1', true, 3000, 1, 1, 1);\n" +
                "\n" +
                "INSERT INTO transactionTbl (amount, dateTime, accountId) VALUES (200, '2008-10-23 10:37:22', 1);\n" +
                "\n" +
                "INSERT INTO transactionCategoryTbl (transactionId, categoryId) VALUES (1, 1);\n" +
                "INSERT INTO transactionCategoryTbl (transactionId, categoryId) VALUES (1, 2);";

    }
}
