package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;

public class AccountRepositoryTest {
    private ConnectionSupplier connectionSupplier;
    private AccountRepository accountRepository;

    private static AccountModel getTestModel() {
        AccountModel accountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Rub", "RU", "R"),
                new AccountTypeModel(1l, "Card"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));
        return accountModel;
    }

    @BeforeEach
    void prepareRepository() {
        connectionSupplier = new ConnectionSupplier();
        SQLquery query = new SQLquery();
        query.createDatabase(connectionSupplier);
        query.fillDatabase(connectionSupplier);
        accountRepository = new AccountRepository(connectionSupplier);
    }

    @Test
    @DisplayName("Account find by ID test")
    void findByIdTest() {
        AccountModel accountModel = getTestModel();
        AccountModel actualAccountModel = accountRepository.findById(1l).orElse(null);
        Assertions.assertEquals(accountModel, actualAccountModel);
    }

    @Test
    @DisplayName("Account find all test")
    void findAllTest() {
        AccountModel accountModel = getTestModel();
        Collection<AccountModel> accountModels = new HashSet<>();
        accountModels.add(accountModel);
        Collection<AccountModel> actualAccountModels = accountRepository.findAll();

        Assertions.assertEquals(accountModels, actualAccountModels);
        Assertions.assertEquals(1, accountRepository.findAll().size());
    }

    @Test
    @DisplayName("Account delete test")
    void deleteTest() {
        TransactionRepository transactionRepository = new TransactionRepository();
        transactionRepository.update(new TransactionModel(1l, new BigDecimal("111"), LocalDateTime.now(),
                null, new HashSet<CategoryTransactionModel>()));

        Assertions.assertEquals(true, accountRepository.delete(1l));
        Assertions.assertEquals(false, accountRepository.delete(100l));
    }

    @Test
    @DisplayName("Account update test")
    void updateTest() {
        AccountModel accountModel = getTestModel();

        AccountModel accountModelException = new AccountModel(null, "null",
                null, null, null,
                true, new BigDecimal("3000.00"));

        AccountModel accountModelException2 = new AccountModel(1l, null,
                null, null, null,
                true, new BigDecimal("3000.00"));

        accountRepository.update(accountModel);
        AccountModel actualAccountModel = accountRepository.findById(1l).orElse(null);
        Assertions.assertEquals(accountModel, actualAccountModel);

        Assertions.assertDoesNotThrow(() -> accountRepository.update(accountModelException));
        Assertions.assertThrows(HomeFinanceDaoException.class, () -> accountRepository.update(accountModelException2));
    }

    @Test
    @DisplayName("Account insert test")
    void insertTest() {
        AccountModel accountModel = new AccountModel("ACCOUNT",
                new CurrencyModel(2l, "Euro", "EU", "E"),
                new AccountTypeModel(2l, "Cash"),
                new BankModel(2l, "alpha"),
                true, new BigDecimal("3000.00"));

        AccountModel accountModelException = new AccountModel( "Acc1",
                new CurrencyModel(1l, "Rub", "RU", "R"),
                new AccountTypeModel(1l, "Card"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        Assertions.assertNotNull(accountRepository.insert(accountModel));
        Assertions.assertThrows(HomeFinanceDaoException.class, () -> accountRepository.insert(accountModelException));
    }
}
