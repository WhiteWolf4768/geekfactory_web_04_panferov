package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;

import java.util.Collection;
import java.util.HashSet;

public class CategoryTransactionRepositoryTest {
    private ConnectionSupplier connectionSupplier;
    private CategoryTransactionRepository categoryTransactionRepository;

    private static CategoryTransactionModel getTestModel() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(2l, "cat2", null);
        return categoryTransactionModel;
    }

    private static Collection<CategoryTransactionModel> getTestModelCollection() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "cat1", null);
        CategoryTransactionModel categoryTransactionModel1 = new CategoryTransactionModel(2l, "cat2", null);
        CategoryTransactionModel categoryTransactionModel2 = new CategoryTransactionModel(3l, "subCat11", categoryTransactionModel);
        CategoryTransactionModel categoryTransactionModel3 = new CategoryTransactionModel(4l, "SUBsubCat111", categoryTransactionModel2);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel);
        categoryTransactionModels.add(categoryTransactionModel1);
        categoryTransactionModels.add(categoryTransactionModel2);
        categoryTransactionModels.add(categoryTransactionModel3);

        return categoryTransactionModels;
    }

    @BeforeEach
    void prepareRepository() {
        connectionSupplier = new ConnectionSupplier();
        SQLquery query = new SQLquery();
        query.createDatabase(connectionSupplier);
        query.fillDatabase(connectionSupplier);
        categoryTransactionRepository = new CategoryTransactionRepository(connectionSupplier);
    }

    @Test
    @DisplayName("CategoryTransaction find by ID test")
    void findByIdTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(2l, "cat2", null);
        CategoryTransactionModel actualCategoryTransactionModel = categoryTransactionRepository.findById(2l).orElse(null);
        Assertions.assertEquals(categoryTransactionModel, actualCategoryTransactionModel);
    }

    @Test
    @DisplayName("CategoryTransaction find all test")
    void findAllTest() {
        Collection<CategoryTransactionModel> categoryTransactionModels = getTestModelCollection();

        Collection<CategoryTransactionModel> actualCategoryTransactionModels = categoryTransactionRepository.findAll();

        Assertions.assertEquals(categoryTransactionModels, actualCategoryTransactionModels);
        Assertions.assertEquals(4, actualCategoryTransactionModels.size());
    }

    @Test
    @DisplayName("CategoryTransaction delete test")
    void deleteTest() {
        Assertions.assertEquals(true, categoryTransactionRepository.delete(4l));
        Assertions.assertThrows(HomeFinanceDaoException.class, () -> categoryTransactionRepository.delete(2l));
        Assertions.assertEquals(false, categoryTransactionRepository.delete(100l));
    }

    @Test
    @DisplayName("CategoryTransaction update test")
    void updateTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(2l, "CAT222", null);
        CategoryTransactionModel categoryTransactionModelException = new CategoryTransactionModel(null,null, null);
        categoryTransactionRepository.update(categoryTransactionModel);
        CategoryTransactionModel actualCategoryTransactionModel = categoryTransactionRepository.findById(2l).orElse(null);
        Assertions.assertEquals(categoryTransactionModel, actualCategoryTransactionModel);

        Assertions.assertDoesNotThrow(() -> categoryTransactionRepository.update(categoryTransactionModelException));
    }

    @Test
    @DisplayName("CategoryTransaction insert test")
    void insertTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel("CAT3", null);
        CategoryTransactionModel categoryTransactionModelException = new CategoryTransactionModel("cat2", null);
        Assertions.assertNotNull(categoryTransactionRepository.insert(categoryTransactionModel));
        Assertions.assertThrows(HomeFinanceDaoException.class, () -> categoryTransactionRepository.insert(categoryTransactionModelException));
    }

    @Test
    @DisplayName("CategoryTransaction find all subcategories test")
    void findAllSubcategoriesTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "cat1", null);
        CategoryTransactionModel categoryTransactionModel2 = new CategoryTransactionModel(3l, "subCat11", categoryTransactionModel);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel2);

        Assertions.assertEquals(categoryTransactionModels, categoryTransactionRepository.findAllSubCategories(1l));
    }
}
