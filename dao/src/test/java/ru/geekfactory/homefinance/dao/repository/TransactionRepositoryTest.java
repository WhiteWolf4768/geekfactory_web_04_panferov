package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.model.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;

public class TransactionRepositoryTest {
    private ConnectionSupplier connectionSupplier;
    private TransactionRepository transactionRepository;

    private static TransactionModel getTestModel() {
        AccountModel accountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Rub", "RU", "R"),
                new AccountTypeModel(1l, "Card"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "cat1", null);
        CategoryTransactionModel categoryTransactionModel1 = new CategoryTransactionModel(2l, "cat2", null);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel);
        categoryTransactionModels.add(categoryTransactionModel1);

        TransactionModel transactionModel = new TransactionModel(1l, new BigDecimal("200.00"), LocalDateTime.of(2008, 10, 23, 10, 37,22), accountModel, categoryTransactionModels);
        return transactionModel;
    }

    @BeforeEach
    void prepareRepository() {
        connectionSupplier = new ConnectionSupplier();
        SQLquery query = new SQLquery();
        query.createDatabase(connectionSupplier);
        query.fillDatabase(connectionSupplier);
        transactionRepository = new TransactionRepository(connectionSupplier);
    }

    @Test
    @DisplayName("Transaction find by ID test")
    void findByIdTest() {
        TransactionModel transactionModel = getTestModel();
        TransactionModel actualTransactionModel = transactionRepository.findById(1l).orElse(null);
        Assertions.assertEquals(transactionModel, actualTransactionModel);
    }

    @Test
    @DisplayName("Transaction find all test")
    void findAllTest() {
        TransactionModel transactionModel = getTestModel();

        Collection<TransactionModel> transactionModels = new HashSet<>();
        transactionModels.add(transactionModel);

        Collection<TransactionModel> actualTransactionModels = transactionRepository.findAll();

        Assertions.assertEquals(transactionModels, actualTransactionModels);
        Assertions.assertEquals(1, actualTransactionModels.size());
    }

    @Test
    @DisplayName("Transaction delete test")
    void deleteTest() {
        TransactionModel transactionModel = new TransactionModel(1l, new BigDecimal("200.00"),
                LocalDateTime.of(2008, 10, 23, 10, 37,22), null, null);
        transactionRepository.update(transactionModel);
        Assertions.assertEquals(true, transactionRepository.delete(1l));
        Assertions.assertEquals(false, transactionRepository.delete(100l));
    }

    @Test
    @DisplayName("Transaction update test")
    void updateTest() {
        AccountModel accountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Rub", "RU", "R"),
                new AccountTypeModel(1l, "Card"),
                new BankModel(1l, "sberbank"),
                true, new BigDecimal("3000.00"));

        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "cat1", null);
        CategoryTransactionModel categoryTransactionModel1 = new CategoryTransactionModel(2l, "cat2", null);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel);
        categoryTransactionModels.add(categoryTransactionModel1);

        TransactionModel transactionModel = new TransactionModel(1l, new BigDecimal("555.00"), LocalDateTime.of(2008, 10, 23, 10, 37,22), accountModel, categoryTransactionModels);
        transactionRepository.update(transactionModel);

        TransactionModel actualTransactionModel = transactionRepository.findById(1l).orElse(null);

        Assertions.assertEquals(transactionModel, actualTransactionModel);

        TransactionModel transactionModelException = new TransactionModel(null, null, null, null);

        Assertions.assertDoesNotThrow(() -> transactionRepository.update(transactionModelException));
    }

    @Test
    @DisplayName("Transaction insert test")
    void insertTest() {
        AccountModel accountModel = new AccountModel(1l, "Acc1",
                new CurrencyModel(1l, "Rub", "RU", "R"),
                new AccountTypeModel(1l, "Card"),
                new BankModel(1l, "sberbank"),
                true, BigDecimal.valueOf(3000));

        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(1l, "cat1", null);
        CategoryTransactionModel categoryTransactionModel1 = new CategoryTransactionModel(2l, "cat2", null);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();
        categoryTransactionModels.add(categoryTransactionModel);
        categoryTransactionModels.add(categoryTransactionModel1);

        TransactionModel transactionModel = new TransactionModel(BigDecimal.valueOf(1000), LocalDateTime.now(), accountModel, categoryTransactionModels);

        Assertions.assertNotNull(transactionRepository.insert(transactionModel));
    }
}
