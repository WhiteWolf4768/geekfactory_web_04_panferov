package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.AccountTypeModel;

import java.util.Collection;
import java.util.HashSet;

public class AccountTypeRepositoryTest {
    private ConnectionSupplier connectionSupplier;
    private AccountTypeRepository accountTypeRepository;

    private static AccountTypeModel getTestModel() {
        AccountTypeModel accountTypeModel = new AccountTypeModel(1l, "Card");
        return accountTypeModel;
    }

    private static Collection<AccountTypeModel> getTestModelCollection() {
        AccountTypeModel accountTypeModel1 = new AccountTypeModel(1l, "Card");
        AccountTypeModel accountTypeModel2 = new AccountTypeModel(2l, "Cash");
        AccountTypeModel accountTypeModel3 = new AccountTypeModel(3l, "Debit");
        Collection<AccountTypeModel> accountTypeModels = new HashSet<>();
        accountTypeModels.add(accountTypeModel1);
        accountTypeModels.add(accountTypeModel2);
        accountTypeModels.add(accountTypeModel3);

        return accountTypeModels;
    }

    @BeforeEach
    void prepareRepository() {
        connectionSupplier = new ConnectionSupplier();
        SQLquery query = new SQLquery();
        query.createDatabase(connectionSupplier);
        query.fillDatabase(connectionSupplier);
        accountTypeRepository = new AccountTypeRepository(connectionSupplier);
    }

    @Test
    @DisplayName("AccountType find by ID test")
    void findByIdTest() {
        AccountTypeModel accountTypeModel = getTestModel();
        AccountTypeModel actualAccountTypeModel = accountTypeRepository.findById(1l).orElse(null);
        Assertions.assertEquals(accountTypeModel, actualAccountTypeModel);
    }

    @Test
    @DisplayName("AccountType find all test")
    void findAllTest() {
        Collection<AccountTypeModel> accountTypeModels = getTestModelCollection();

        Collection<AccountTypeModel> actualAccountTypeModels = accountTypeRepository.findAll();

        Assertions.assertEquals(accountTypeModels, actualAccountTypeModels);
        Assertions.assertEquals(3, accountTypeRepository.findAll().size());
    }

    @Test
    @DisplayName("AccountType delete test")
    void deleteTest() {
        Assertions.assertEquals(true, accountTypeRepository.delete(2l));
        Assertions.assertEquals(2, accountTypeRepository.findAll().size());
        Assertions.assertEquals(false, accountTypeRepository.delete(100l));
    }

    @Test
    @DisplayName("AccountType update test")
    void updateTest() {
        AccountTypeModel accountTypeModel = new AccountTypeModel(2l, "CASH");
        AccountTypeModel accountTypeModelException = new AccountTypeModel(null, "null");
        accountTypeRepository.update(accountTypeModel);
        AccountTypeModel actualAccountTypeModel = accountTypeRepository.findById(2l).orElse(null);
        Assertions.assertEquals(accountTypeModel, actualAccountTypeModel);

        Assertions.assertDoesNotThrow(() -> accountTypeRepository.update(accountTypeModelException));
    }

    @Test
    @DisplayName("AccountType insert test")
    void insertTest() {
        AccountTypeModel accountTypeModel = new AccountTypeModel("CREDIT");
        AccountTypeModel accountTypeModelException = new AccountTypeModel("Cash");
        Assertions.assertNotNull(accountTypeRepository.insert(accountTypeModel));
        Assertions.assertThrows(HomeFinanceDaoException.class, () -> accountTypeRepository.insert(accountTypeModelException));
    }
}
