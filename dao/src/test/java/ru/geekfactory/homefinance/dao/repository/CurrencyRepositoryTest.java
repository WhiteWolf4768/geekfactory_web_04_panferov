package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import java.util.Collection;
import java.util.HashSet;

public class CurrencyRepositoryTest {

    private ConnectionSupplier connectionSupplier;
    private CurrencyRepository currencyRepository;

    private static CurrencyModel getTestModel() {
        CurrencyModel currencyModel = new CurrencyModel(1l, "Rub", "RU", "R");
        return currencyModel;
    }

    private static Collection<CurrencyModel> getTestModelCollection() {
        CurrencyModel currencyModel1 = new CurrencyModel(1l, "Rub", "RU", "R");
        CurrencyModel currencyModel2 = new CurrencyModel(2l, "Euro", "EU", "E");
        Collection<CurrencyModel> currencyModels = new HashSet<>();
        currencyModels.add(currencyModel1);
        currencyModels.add(currencyModel2);

        return currencyModels;
    }

    @BeforeEach
    void prepareRepository() {
        connectionSupplier = new ConnectionSupplier();
        SQLquery query = new SQLquery();
        query.createDatabase(connectionSupplier);
        query.fillDatabase(connectionSupplier);
        currencyRepository = new CurrencyRepository(connectionSupplier);
    }

    @Test
    @DisplayName("Currency find by ID test")
    void findByIdTest() {
        CurrencyModel currencyModel = getTestModel();
        CurrencyModel actualCurrencyModel = currencyRepository.findById(1l).orElse(null);
        Assertions.assertEquals(currencyModel, actualCurrencyModel);
    }

    @Test
    @DisplayName("Currency find all test")
    void findAllTest() {
        Collection<CurrencyModel> currencyModels = getTestModelCollection();

        Collection<CurrencyModel> actualCurrencyModels = currencyRepository.findAll();

        Assertions.assertEquals(currencyModels, actualCurrencyModels);
        Assertions.assertEquals(2, currencyRepository.findAll().size());
    }

    @Test
    @DisplayName("Currency delete test")
    void deleteTest() {
        Assertions.assertEquals(true, currencyRepository.delete(2l));
        Assertions.assertEquals(1, currencyRepository.findAll().size());
        Assertions.assertEquals(false, currencyRepository.delete(100l));
    }

    @Test
    @DisplayName("Currency update test")
    void updateTest() {
        CurrencyModel currencyModel = new CurrencyModel(2l, "Dollar", "D", "$");
        CurrencyModel currencyModelException = new CurrencyModel(null, "null", "null", "null");
        currencyRepository.update(currencyModel);
        CurrencyModel actualCurrencyModel = currencyRepository.findById(2l).orElse(null);
        Assertions.assertEquals(currencyModel, actualCurrencyModel);

        Assertions.assertDoesNotThrow(() -> currencyRepository.update(currencyModelException));
    }

    @Test
    @DisplayName("Currency insert test")
    void insertTest() {
        CurrencyModel currencyModel = new CurrencyModel("Ruby", "RB", "R");
        CurrencyModel currencyModelException = new CurrencyModel("Rub", "RU", "R");
        Assertions.assertNotNull(currencyRepository.insert(currencyModel));
        Assertions.assertThrows(HomeFinanceDaoException.class, () -> currencyRepository.insert(currencyModelException));
    }

}
