package ru.geekfactory.homefinance.dao.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.BankModel;

import java.util.Collection;
import java.util.HashSet;

public class BankRepositoryTest {
    private ConnectionSupplier connectionSupplier;
    private BankRepository bankRepository;

    private static BankModel getTestModel() {
        BankModel bankModel = new BankModel(1l, "sberbank");
        return bankModel;
    }

    private static Collection<BankModel> getTestModelCollection() {
        BankModel bankModel1 = new BankModel(1l, "sberbank");
        BankModel bankModel2 = new BankModel(2l, "alpha");
        BankModel bankModel3 = new BankModel(3l, "nya");
        Collection<BankModel> bankModels = new HashSet<>();
        bankModels.add(bankModel1);
        bankModels.add(bankModel2);
        bankModels.add(bankModel3);

        return bankModels;
    }

    @BeforeEach
    void prepareTest() {
        connectionSupplier = new ConnectionSupplier();
        SQLquery query = new SQLquery();
        query.createDatabase(connectionSupplier);
        query.fillDatabase(connectionSupplier);
        bankRepository = new BankRepository(connectionSupplier);
    }

    @Test
    @DisplayName("Bank find by ID test")
    void findByIdTest() {
        BankModel bankModel = getTestModel();
        BankModel actualBankModel = bankRepository.findById(1l).orElse(null);
        Assertions.assertEquals(bankModel, actualBankModel);
    }

    @Test
    @DisplayName("Bank find all test")
    void findAllTest() {
        Collection<BankModel> bankModels = getTestModelCollection();

        Collection<BankModel> actualBankModels = bankRepository.findAll();

        Assertions.assertEquals(bankModels, actualBankModels);
        Assertions.assertEquals(3, bankRepository.findAll().size());
    }

    @Test
    @DisplayName("Bank delete test")
    void deleteTest() {
        Assertions.assertEquals(true, bankRepository.delete(2l));
        Assertions.assertEquals(2, bankRepository.findAll().size());
        Assertions.assertEquals(false, bankRepository.delete(100l));
    }

    @Test
    @DisplayName("Bank update test")
    void updateTest() {
        BankModel bankModel = new BankModel(2l, "SBERBANK");
        BankModel bankModelException = new BankModel(null, "null");
        bankRepository.update(bankModel);

        BankModel actualBankModel = bankRepository.findById(2l).orElse(null);

        Assertions.assertEquals(bankModel, actualBankModel);

        Assertions.assertDoesNotThrow(() -> bankRepository.update(bankModelException));
    }

    @Test
    @DisplayName("Bank insert test")
    void insertTest() {
        BankModel bankModel = new BankModel("BANK");
        BankModel bankModelException = new BankModel("alpha");
        Assertions.assertNotNull(bankRepository.insert(bankModel));
        Assertions.assertThrows(HomeFinanceDaoException.class, () -> bankRepository.insert(bankModelException));
    }
}
