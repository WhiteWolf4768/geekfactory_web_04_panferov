package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionSupplier {
    private Properties properties;
    private InputStream inputStream;

    private String dbUrl;
    private String dbUser;
    private String dbPass;

    public ConnectionSupplier() {
        properties = new Properties();
        inputStream = getClass().getClassLoader().getResourceAsStream("database.connection.properties");

        try {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new HomeFinanceDaoException("Load database properties error", e);
        }

        this.dbUrl = properties.getProperty("dburl");
        this.dbUser = properties.getProperty("dbuser");
        this.dbPass = properties.getProperty("dbpass");
    }

    public Connection getConnection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPass);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("Error with DB connection", e);
        } catch (ClassNotFoundException e) {
            throw new HomeFinanceDaoException("Class com.mysql.jdbc.Driver not found", e);
        }
    }
}
