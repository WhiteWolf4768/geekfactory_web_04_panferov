package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;

import java.sql.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class CurrencyRepository implements Repository<CurrencyModel> {
    private final static String INSERT = "INSERT INTO currencyTbl (name, code, symbol) VALUES (?, ?, ?)";
    private final static String DELETE = "DELETE FROM currencyTbl WHERE id = ?";
    private final static String SELECT_BY_ID = "SELECT id, name, code, symbol FROM currencyTbl WHERE id = ?";
    private final static String SELECT_ALL = "SELECT id, name, code, symbol FROM currencyTbl";
    private final static String UPDATE = "UPDATE currencyTbl SET name = ?, code = ?, symbol = ? WHERE id = ?";
    private ConnectionSupplier connectionSupplier;

    public CurrencyRepository() {
        this.connectionSupplier = new ConnectionSupplier();
    }

    public CurrencyRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public CurrencyModel insert(CurrencyModel model) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.setString(2, model.getCode());
                preparedStatement.setString(3, model.getSymbol());
                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }

                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("CurrencyModel INSERT error" + model, e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CurrencyModel INSERT error" + model, e);
        }
    }

    @Override
    public Optional<CurrencyModel> findById(Long id) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeQuery();

                CurrencyModel model = null;

                ResultSet resultSet = preparedStatement.getResultSet();
                if (resultSet.next()) {
                    Long currencyId = resultSet.getLong(1);
                    String name = resultSet.getString(2);
                    String code = resultSet.getString(3);
                    String symbol = resultSet.getString(4);

                    model = new CurrencyModel(currencyId, name, code, symbol);
                }

                return Optional.ofNullable(model);
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CurrencyModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CurrencyModel find error", e);
        }
    }

    @Override
    public Collection<CurrencyModel> findAll() {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
                preparedStatement.executeQuery();

                HashSet<CurrencyModel> currencyModelHashSet = new HashSet<>();
                CurrencyModel model;

                ResultSet resultSet = preparedStatement.getResultSet();
                while (resultSet.next()) {
                    Long currencyId = resultSet.getLong(1);
                    String name = resultSet.getString(2);
                    String code = resultSet.getString(3);
                    String symbol = resultSet.getString(4);

                    model = new CurrencyModel(currencyId, name, code, symbol);
                    currencyModelHashSet.add(model);
                }

                return currencyModelHashSet;
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CurrencyModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CurrencyModel find error", e);
        }
    }

    @Override
    public void update(CurrencyModel model) {
        if (model.getId() != null) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                    preparedStatement.setString(1, model.getName());
                    preparedStatement.setString(2, model.getCode());
                    preparedStatement.setString(3, model.getSymbol());
                    preparedStatement.setLong(4, model.getId());
                    preparedStatement.executeUpdate();

                    connection.commit();
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("CurrencyModel UPDATE error" + model, e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CurrencyModel UPDATE error" + model, e);
            }
        } else {
            System.out.println("Model id is NULL!");
        }
    }

    @Override
    public boolean delete(Long id) {
        if (findById(id).isPresent()) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
                    preparedStatement.setLong(1, id);
                    preparedStatement.executeUpdate();

                    connection.commit();
                    return true;
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("CurrencyModel DELETE error", e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CurrencyModel DELETE error", e);
            }
        } else {
            return false;
        }
    }
}
