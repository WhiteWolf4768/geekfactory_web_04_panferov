package ru.geekfactory.homefinance.dao.repository;

import java.util.Collection;
import java.util.Optional;

public interface Repository<T> {
    Optional<T> findById(Long id);
    Collection<T> findAll();
    T insert(T model);
    void update(T model);
    boolean delete(Long id);
}
