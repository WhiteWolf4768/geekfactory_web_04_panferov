package ru.geekfactory.homefinance.dao.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    List<TransactionModel> transactionModelList = new ArrayList<>();
    List<CategoryTransactionModel> categoryTransactionModelList;
    TransactionModel transactionModel = null;
    AccountModel accountModel = null;
    CategoryTransactionModel categoryTransactionModel = null;
    String content = null;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch(qName){
            case "transaction":
                transactionModel = new TransactionModel();
                transactionModel.setId(Long.parseLong(attributes.getValue("id")));
                break;
            case "accountModel":
                accountModel = new AccountModel();
                accountModel.setId(Long.parseLong(attributes.getValue("id")));
                break;
            case "category":
                categoryTransactionModel = new CategoryTransactionModel();
                categoryTransactionModel.setId(Long.parseLong(attributes.getValue("id")));
                break;
            case "categories":
                categoryTransactionModelList = new ArrayList<>();
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        switch(qName){
            case "amount":
                transactionModel.setAmount(new BigDecimal(content));
                break;
            case "dateTime":
                transactionModel.setDateTime(LocalDateTime.parse(content, formatter));
                break;
            case "accountName":
                accountModel.setName(content);
                break;
            case "accountIsActive":
                accountModel.setActive(Boolean.parseBoolean(content));
                break;
            case "accountAmount":
                accountModel.setAmount(new BigDecimal(content));
                break;
            case "categoryName":
                categoryTransactionModel.setName(content);
                categoryTransactionModelList.add(categoryTransactionModel);
                break;
            case "transaction":
                transactionModel.setAccountModel(accountModel);
                transactionModel.setCategoryTransactionModel(categoryTransactionModelList);
                transactionModelList.add(transactionModel);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        content = String.copyValueOf(ch, start, length).trim();
    }
}
