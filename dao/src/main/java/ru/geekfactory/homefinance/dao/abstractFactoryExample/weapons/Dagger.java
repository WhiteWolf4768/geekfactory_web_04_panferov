package ru.geekfactory.homefinance.dao.abstractFactoryExample.weapons;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractWeapon;

public class Dagger extends AbstractWeapon {
    public Dagger() {
        damage = 8;
    }

    @Override
    public int getDamage() {
        return this.damage;
    }
}
