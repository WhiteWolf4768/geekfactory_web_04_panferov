package ru.geekfactory.homefinance.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CurrencyModel {
    private Long id;
    private String name;
    private String code;
    private String symbol;

    public CurrencyModel(String name, String code, String symbol) {
        this.name = name;
        this.code = code;
        this.symbol = symbol;
    }
}

