package ru.geekfactory.homefinance.dao.abstractFactoryExample;

public abstract class CatFighterAbstractFactory {
    public abstract AbstractCharacterClass createClass();
    public abstract AbstractArmor createArmor();
    public abstract AbstractWeapon createWeapon();
}
