package ru.geekfactory.homefinance.dao.abstractFactoryExample.classes;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractCharacterClass;

public class Rouge extends AbstractCharacterClass {
    public Rouge() {
        className = "Rouge";
    }

    @Override
    public String getClassName() {
        return this.className;
    }
}
