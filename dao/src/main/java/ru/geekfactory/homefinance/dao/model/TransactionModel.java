package ru.geekfactory.homefinance.dao.model;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionModel {
    private Long id;
    private BigDecimal amount;
    private LocalDateTime dateTime;
    private AccountModel accountModel;
    private Collection<CategoryTransactionModel> categoryTransactionModel;

    public TransactionModel(BigDecimal amount, LocalDateTime dateTime, AccountModel accountModel, Collection<CategoryTransactionModel> categoryTransactionModel) {
        this.amount = amount;
        this.dateTime = dateTime;
        this.accountModel = accountModel;
        this.categoryTransactionModel = categoryTransactionModel;
    }
}
