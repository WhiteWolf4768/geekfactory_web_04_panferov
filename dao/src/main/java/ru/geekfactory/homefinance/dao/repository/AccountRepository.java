package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.AccountTypeModel;
import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class AccountRepository implements Repository<AccountModel> {
    private final static String INSERT = "INSERT INTO accountTbl (name, isActive, amount, currencyId, accountTypeId, bankId)" +
            " VALUES (?, ?, ?, ?, ?, ?)";
    private final static String DELETE = "DELETE FROM accountTbl WHERE id = ?";
    private final static String SELECT_BY_ID = "SELECT acc.id, acc.name, acc.isActive, acc.amount,\n" +
            "acc.currencyId, cur.name, cur.code, cur.symbol,\n" +
            "acc.accountTypeId, accType.name,\n" +
            "acc.bankId, bank.name\n" +
            "FROM accountTbl as acc\n" +
            "JOIN currencyTbl as cur on acc.currencyId = cur.id\n" +
            "JOIN accountTypeTbl as accType on acc.accountTypeId = accType.id\n" +
            "JOIN bankTbl as bank on acc.bankId = bank.id WHERE acc.id = ?";
    private final static String SELECT_ALL = "SELECT acc.id, acc.name, acc.isActive, acc.amount,\n" +
            "acc.currencyId, cur.name, cur.code, cur.symbol,\n" +
            "acc.accountTypeId, accType.name,\n" +
            "acc.bankId, bank.name\n" +
            "FROM accountTbl as acc\n" +
            "JOIN currencyTbl as cur on acc.currencyId = cur.id\n" +
            "JOIN accountTypeTbl as accType on acc.accountTypeId = accType.id\n" +
            "JOIN bankTbl as bank on acc.bankId = bank.id";
    private final static String UPDATE = "UPDATE accountTbl SET name = ?, isActive = ?, amount = ?, currencyId = ?," +
            "accountTypeId = ?, bankId = ? WHERE id = ?";
    private ConnectionSupplier connectionSupplier;

    public AccountRepository() {
        this.connectionSupplier = new ConnectionSupplier();
    }

    public AccountRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Optional<AccountModel> findById(Long id) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeQuery();

                AccountModel model = null;

                ResultSet resultSet = preparedStatement.getResultSet();
                if (resultSet.next()) {
                    Long accountId = resultSet.getLong(1);
                    String name = resultSet.getString(2);
                    boolean isActive = resultSet.getBoolean(3);
                    BigDecimal amount = resultSet.getBigDecimal(4);

                    CurrencyModel currency = new CurrencyModel(
                            resultSet.getLong(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8));

                    AccountTypeModel accountType = new AccountTypeModel(resultSet.getLong(9), resultSet.getString(10));
                    BankModel bank = new BankModel(resultSet.getLong(11), resultSet.getString(12));

                    model = new AccountModel(accountId, name, currency, accountType, bank, isActive, amount);
                }

                return Optional.ofNullable(model);
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("AccountModel find error", e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Collection<AccountModel> findAll() {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
                preparedStatement.executeQuery();

                HashSet accountModelHashSet = new HashSet();
                AccountModel model;

                ResultSet resultSet = preparedStatement.getResultSet();
                while (resultSet.next()) {
                    Long accountId = resultSet.getLong(1);
                    String name = resultSet.getString(2);
                    boolean isActive = resultSet.getBoolean(3);
                    BigDecimal amount = resultSet.getBigDecimal(4);

                    CurrencyModel currency = new CurrencyModel(
                            resultSet.getLong(5),
                            resultSet.getString(6),
                            resultSet.getString(7),
                            resultSet.getString(8));

                    AccountTypeModel accountType = new AccountTypeModel(resultSet.getLong(9), resultSet.getString(10));
                    BankModel bank = new BankModel(resultSet.getLong(11), resultSet.getString(12));

                    model = new AccountModel(accountId, name, currency, accountType, bank, isActive, amount);
                    accountModelHashSet.add(model);
                }

                return accountModelHashSet;
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("AccountModel find error", e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public AccountModel insert(AccountModel model) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.setBoolean(2, model.isActive());
                preparedStatement.setBigDecimal(3, model.getAmount());

                if (isCurrencyExist(model)) {
                    preparedStatement.setLong(4, model.getCurrencyModel().getId());
                } else {
                    preparedStatement.setString(4, null);
                }

                if (isAccountTypeExist(model)) {
                    preparedStatement.setLong(5, model.getAccountTypeModel().getId());
                } else {
                    preparedStatement.setString(5, null);
                }

                if (isBankExist(model)) {
                    preparedStatement.setLong(6, model.getBankModel().getId());
                } else {
                    preparedStatement.setString(6, null);
                }

                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }

                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("AccountModel INSERT error" + model, e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("AccountModel INSERT error" + model, e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void update(AccountModel model) {
        if (model.getId() != null) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                    preparedStatement.setString(1, model.getName());
                    preparedStatement.setBoolean(2, model.isActive());
                    preparedStatement.setBigDecimal(3, model.getAmount());

                    if (isCurrencyExist(model)) {
                        preparedStatement.setLong(4, model.getCurrencyModel().getId());
                    } else {
                        preparedStatement.setString(4, null);
                    }

                    if (isAccountTypeExist(model)) {
                        preparedStatement.setLong(5, model.getAccountTypeModel().getId());
                    } else {
                        preparedStatement.setString(5, null);
                    }

                    if (isBankExist(model)) {
                        preparedStatement.setLong(6, model.getBankModel().getId());
                    } else {
                        preparedStatement.setString(6, null);
                    }

                    preparedStatement.setLong(7, model.getId());
                    preparedStatement.executeUpdate();

                    connection.commit();
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("AccountModel UPDATE error" + model, e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountModel UPDATE error" + model, e);
            }
        } else {
            System.out.println("Model id is NULL!");
        }
    }

    @Override
    public boolean delete(Long id) {
        if (findById(id).isPresent()) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
                    preparedStatement.setLong(1, id);
                    preparedStatement.executeUpdate();

                    connection.commit();
                    return true;
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("AccountModel DELETE error", e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountModel DELETE error", e);
            }
        } else {
            return false;
        }
    }

    private static boolean isCurrencyExist(AccountModel model) {
        if (model.getCurrencyModel() != null) {
            if (model.getCurrencyModel().getId() != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static boolean isAccountTypeExist(AccountModel model) {
        if (model.getAccountTypeModel() != null) {
            if (model.getAccountTypeModel().getId() != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static boolean isBankExist(AccountModel model) {
        if (model.getBankModel() != null) {
            if (model.getBankModel().getId() != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
