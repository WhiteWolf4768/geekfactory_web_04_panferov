package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class TransactionRepository implements Repository<TransactionModel> {
    private final static String INSERT = "INSERT INTO transactionTbl (amount, dateTime, accountId) VALUES (?, ?, ?)";
    private final static String DELETE = "DELETE FROM transactionTbl WHERE id = ?";
    private final static String SELECT_BY_ID = "SELECT id, amount, dateTime, accountId FROM transactionTbl WHERE id = ?";
    private final static String SELECT_ALL = "SELECT id, amount, dateTime, accountId FROM transactionTbl";
    private final static String UPDATE = "UPDATE transactionTbl SET amount = ?, dateTime = ?, accountId = ? WHERE id = ?";
    private final static String SELECT_CATEGORIES_BY_ID = "SELECT id, transactionId, categoryId FROM transactionCategoryTbl WHERE transactionId = ?";
    private final static String INSERT_TRANSACTION_CATEGORY = "INSERT INTO transactionCategoryTbl (transactionId, categoryId) VALUES (?, ?)";
    private final static String DELETE_ALL_CATEGORIES = "DELETE FROM transactionCategoryTbl WHERE transactionId = ?";
    private ConnectionSupplier connectionSupplier;
    private AccountRepository accountRepository;
    private CategoryTransactionRepository categoryTransactionRepository;

    public TransactionRepository() {
        this.connectionSupplier = new ConnectionSupplier();
        this.accountRepository = new AccountRepository();
        this.categoryTransactionRepository = new CategoryTransactionRepository();
    }

    public TransactionRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        this.accountRepository = new AccountRepository(connectionSupplier);
        this.categoryTransactionRepository = new CategoryTransactionRepository(connectionSupplier);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Optional<TransactionModel> findById(Long id) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeQuery();

                TransactionModel model = null;

                ResultSet resultSet = preparedStatement.getResultSet();
                if (resultSet.next()) {
                    Long transactionId = resultSet.getLong(1);
                    BigDecimal amount = resultSet.getBigDecimal(2);
                    LocalDateTime dateTime = resultSet.getObject(3, LocalDateTime.class);

                    AccountModel accountModel = accountRepository.findById(resultSet.getLong(4)).orElse(null);

                    model = new TransactionModel(transactionId, amount, dateTime, accountModel, null);
                }

                if (model != null) {
                    model.setCategoryTransactionModel(getCategories(connection, model.getId()));
                }

                return Optional.ofNullable(model);
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("TransactionModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("TransactionModel find error", e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Collection<TransactionModel> findAll() {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
                preparedStatement.executeQuery();

                TransactionModel model;
                HashSet<TransactionModel> transactionModelHashSet = new HashSet<>();

                ResultSet resultSet = preparedStatement.getResultSet();
                while (resultSet.next()) {
                    Long transactionId = resultSet.getLong(1);
                    BigDecimal amount = resultSet.getBigDecimal(2);
                    LocalDateTime dateTime = resultSet.getObject(3, LocalDateTime.class);

                    AccountModel accountModel = accountRepository.findById(resultSet.getLong(4)).orElse(null);

                    model = new TransactionModel(transactionId, amount, dateTime, accountModel, null);
                    transactionModelHashSet.add(model);
                }

                HashSet<TransactionModel> finalSet = new HashSet<>();
                for (TransactionModel models: transactionModelHashSet) {
                    if (models != null) {
                        models.setCategoryTransactionModel(getCategories(connection, models.getId()));
                        finalSet.add(models);
                    }
                }

                return finalSet;
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("TransactionModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("TransactionModel find error", e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public TransactionModel insert(TransactionModel model) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setBigDecimal(1, model.getAmount());
                preparedStatement.setTimestamp(2, java.sql.Timestamp.valueOf(model.getDateTime()));

                if (isAccountExist(model)) {
                    preparedStatement.setLong(3, model.getAccountModel().getId());
                } else {
                    preparedStatement.setString(3, null);
                }

                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }

                for (CategoryTransactionModel setElement: model.getCategoryTransactionModel()) {
                    insertCategory(connection, model.getId(), setElement.getId());
                }

                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("TransactionModel INSERT error" + model, e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("TransactionModel INSERT error" + model, e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void update(TransactionModel model) {
        if (model.getId() != null) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                    preparedStatement.setBigDecimal(1, model.getAmount());
                    preparedStatement.setTimestamp(2, java.sql.Timestamp.valueOf(model.getDateTime()));

                    if (isAccountExist(model)) {
                        preparedStatement.setLong(3, model.getAccountModel().getId());
                    } else {
                        preparedStatement.setString(3, null);
                    }

                    preparedStatement.setLong(4, model.getId());
                    preparedStatement.executeUpdate();

                    clearCategories(connection, model.getId());

                    if (model.getCategoryTransactionModel() != null) {
                        for (CategoryTransactionModel setElement: model.getCategoryTransactionModel()) {
                            insertCategory(connection, model.getId(), setElement.getId());
                        }
                    }

                    connection.commit();
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("AccountModel UPDATE error" + model, e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountModel UPDATE error" + model, e);
            }
        } else {
            System.out.println("Model id is NULL!");
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean delete(Long id) {
        if (findById(id).isPresent()) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
                    preparedStatement.setLong(1, id);
                    preparedStatement.executeUpdate();

                    connection.commit();
                    return true;
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("TransactionModel DELETE error", e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("TransactionModel DELETE error", e);
            }
        } else {
            return false;
        }
    }

    private static boolean isAccountExist(TransactionModel model) {
        if (model.getAccountModel() != null) {
            if (model.getAccountModel().getId() != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private CategoryTransactionModel insertCategory(Connection connection, Long transactionId, Long categoryId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TRANSACTION_CATEGORY)) {
            preparedStatement.setLong(1, transactionId);
            preparedStatement.setLong(2, categoryId);
            preparedStatement.executeUpdate();

            CategoryTransactionModel model = categoryTransactionRepository.findById(categoryId).orElse(null);

            return model;
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CategoryTransactionModel insert categories error", e);
        }
    }

    public Collection<CategoryTransactionModel> getCategories(Connection connection, Long transactionId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CATEGORIES_BY_ID)) {
            preparedStatement.setLong(1, transactionId);
            preparedStatement.executeQuery();

            HashSet<CategoryTransactionModel> categoryTransactionHashSet = new HashSet<>();
            CategoryTransactionModel model;

            ResultSet resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                Long categoryId = resultSet.getLong(3);

                model = categoryTransactionRepository.findById(categoryId).orElse(null);

                categoryTransactionHashSet.add(model);
            }
            if (categoryTransactionHashSet.isEmpty()) {
                return null;
            } else {
                return categoryTransactionHashSet;
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CategoryTransactionModel select all error", e);
        }
    }

    @SuppressWarnings("Duplicates")
    public boolean clearCategories(Connection connection, Long transactionId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ALL_CATEGORIES)) {
            preparedStatement.setLong(1, transactionId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CategoryTransactionModel DELETE error", e);
        }
    }
}
