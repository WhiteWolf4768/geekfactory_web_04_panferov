package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.AccountTypeModel;

import java.sql.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class AccountTypeRepository implements Repository<AccountTypeModel> {
    private final static String INSERT = "INSERT INTO accountTypeTbl (name) VALUES (?)";
    private final static String DELETE = "DELETE FROM accountTypeTbl WHERE id = ?";
    private final static String SELECT_BY_ID = "SELECT id, name FROM accountTypeTbl WHERE id = ?";
    private final static String SELECT_ALL = "SELECT id, name FROM accountTypeTbl";
    private final static String UPDATE = "UPDATE accountTypeTbl SET name = ? WHERE id = ?";
    private ConnectionSupplier connectionSupplier;

    public AccountTypeRepository() {
        this.connectionSupplier = new ConnectionSupplier();
    }

    public AccountTypeRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public Optional<AccountTypeModel> findById(Long id) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeQuery();

                AccountTypeModel model = null;

                ResultSet resultSet = preparedStatement.getResultSet();
                if (resultSet.next()) {
                    Long typeId = resultSet.getLong(1);
                    String name = resultSet.getString(2);

                    model = new AccountTypeModel(typeId, name);
                }

                return Optional.ofNullable(model);
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountTypeModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("AccountTypeModel find error", e);
        }
    }

    @Override
    public Collection<AccountTypeModel> findAll() {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
                preparedStatement.executeQuery();
                HashSet<AccountTypeModel> accountTypeModelHashSet = new HashSet<>();
                AccountTypeModel model;

                ResultSet resultSet = preparedStatement.getResultSet();
                while (resultSet.next()) {
                    Long typeId = resultSet.getLong(1);
                    String name = resultSet.getString(2);

                    model = new AccountTypeModel(typeId, name);
                    accountTypeModelHashSet.add(model);
                }

                return accountTypeModelHashSet;
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountTypeModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("AccountTypeModel find error", e);
        }
    }

    @Override
    public AccountTypeModel insert(AccountTypeModel model) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getType());
                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }

                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("AccountTypeModel INSERT error" + model, e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("AccountTypeModel INSERT error" + model, e);
        }
    }

    @Override
    public void update(AccountTypeModel model) {
        if (model.getId() != null) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                    preparedStatement.setString(1, model.getType());
                    preparedStatement.setLong(2, model.getId());
                    preparedStatement.executeUpdate();

                    connection.commit();
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("AccountTypeModel UPDATE error" + model, e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountTypeModel UPDATE error" + model, e);
            }
        } else {
            System.out.println("Model id is NULL!");
        }
    }

    @Override
    public boolean delete(Long id) {
        if (findById(id).isPresent()) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
                    preparedStatement.setLong(1, id);
                    preparedStatement.executeUpdate();

                    connection.commit();
                    return true;
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("AccountTypeModel DELETE error", e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("AccountTypeModel DELETE error", e);
            }
        } else {
            return false;
        }
    }
}
