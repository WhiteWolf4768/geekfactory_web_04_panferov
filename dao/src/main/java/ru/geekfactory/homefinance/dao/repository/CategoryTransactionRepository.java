package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;

import java.sql.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class CategoryTransactionRepository implements Repository<CategoryTransactionModel> {
    private final static String INSERT = "INSERT INTO categoryTbl (name, parentCategoryId) VALUES (?, ?)";
    private final static String DELETE = "DELETE FROM categoryTbl WHERE id = ?";
    private final static String SELECT_BY_ID = "SELECT id, name, parentCategoryId FROM categoryTbl WHERE id = ?";
    private final static String SELECT_ALL = "SELECT id, name, parentCategoryId FROM categoryTbl";
    private final static String UPDATE = "UPDATE categoryTbl SET name = ?, parentCategoryId = ? WHERE id = ?";
    private final static String SELECT_ALL_SUB_CATEGORIES = "SELECT id, name, parentCategoryId FROM categoryTbl WHERE parentCategoryId = ?";
    private ConnectionSupplier connectionSupplier;

    public CategoryTransactionRepository() {
        this.connectionSupplier = new ConnectionSupplier();
    }

    public CategoryTransactionRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public Optional<CategoryTransactionModel> findById(Long id) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeQuery();

                CategoryTransactionModel model = null;

                ResultSet resultSet = preparedStatement.getResultSet();
                if (resultSet.next()) {
                    Long categoryId = resultSet.getLong(1);
                    String name = resultSet.getString(2);

                    CategoryTransactionModel parentCategoryTransactionModel = findById(resultSet.getLong(3)).orElse(null);

                    model = new CategoryTransactionModel(categoryId, name, parentCategoryTransactionModel);
                }

                return Optional.ofNullable(model);
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CategoryTransactionModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CategoryTransactionModel find error", e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Collection<CategoryTransactionModel> findAll() {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
                preparedStatement.executeQuery();

                HashSet<CategoryTransactionModel> categoryTransactionHashSet = new HashSet<>();
                CategoryTransactionModel model;

                ResultSet resultSet = preparedStatement.getResultSet();
                while (resultSet.next()) {
                    Long categoryId = resultSet.getLong(1);
                    String name = resultSet.getString(2);

                    CategoryTransactionModel parentCategoryTransactionModel = findById(resultSet.getLong(3)).orElse(null);

                    model = new CategoryTransactionModel(categoryId, name, parentCategoryTransactionModel);
                    categoryTransactionHashSet.add(model);
                }

                return categoryTransactionHashSet;
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CategoryTransactionModel select all error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CategoryTransactionModel select all error", e);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public CategoryTransactionModel insert(CategoryTransactionModel model) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());

                if (isParentCategoryExist(model)) {
                    preparedStatement.setLong(2, model.getParentCategoryTransactionModel().getId());
                } else {
                    preparedStatement.setString(2, null);
                }

                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }

                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("CategoryTransactionModel INSERT error" + model, e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("CategoryTransactionModel INSERT error" + model, e);
        }
    }

    @Override
    public void update(CategoryTransactionModel model) {
        if (model.getId() != null) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                    preparedStatement.setString(1, model.getName());

                    if (isParentCategoryExist(model)) {
                        preparedStatement.setLong(2, model.getParentCategoryTransactionModel().getId());
                    } else {
                        preparedStatement.setString(2, null);
                    }

                    preparedStatement.setLong(3, model.getId());
                    preparedStatement.executeUpdate();

                    connection.commit();
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("CategoryTransactionModel UPDATE error" + model, e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CategoryTransactionModel UPDATE error" + model, e);
            }
        } else {
            System.out.println("Model id is NULL!");
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean delete(Long id) {
        if (findById(id).isPresent()) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
                    preparedStatement.setLong(1, id);
                    preparedStatement.executeUpdate();

                    connection.commit();
                    return true;
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("CategoryTransactionModel DELETE error", e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CategoryTransactionModel DELETE error", e);
            }
        } else {
            return false;
        }
    }

    public Collection<CategoryTransactionModel> findAllSubCategories(Long id) {
        if (id != null) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_SUB_CATEGORIES)) {
                    preparedStatement.setLong(1, id);
                    preparedStatement.executeQuery();

                    HashSet<CategoryTransactionModel> CategoryTransactionHashSet = new HashSet<>();
                    CategoryTransactionModel model;

                    ResultSet resultSet = preparedStatement.getResultSet();
                    while (resultSet.next()) {
                        Long categoryId = resultSet.getLong(1);
                        String name = resultSet.getString(2);

                        CategoryTransactionModel parentCategoryTransactionModel = findById(resultSet.getLong(3)).orElse(null);

                        model = new CategoryTransactionModel(categoryId, name, parentCategoryTransactionModel);
                        CategoryTransactionHashSet.add(model);
                    }

                    return CategoryTransactionHashSet;
                } catch (SQLException e) {
                    throw new HomeFinanceDaoException("CategoryTransactionModel select all subcategories error", e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("CategoryTransactionModel select all subcategories error", e);
            }
        } else {
            return null;
        }
    }

    private static boolean isParentCategoryExist(CategoryTransactionModel model) {
        if (model.getParentCategoryTransactionModel() != null) {
            if (model.getParentCategoryTransactionModel().getId() != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
