package ru.geekfactory.homefinance.dao.abstractFactoryExample;

public abstract class AbstractCharacterClass {
    protected String className;

    public AbstractCharacterClass() {
        this.className = className;
    }

    public abstract String getClassName();
}
