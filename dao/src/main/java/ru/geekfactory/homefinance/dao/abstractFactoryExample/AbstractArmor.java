package ru.geekfactory.homefinance.dao.abstractFactoryExample;

public abstract class AbstractArmor {
    protected int armor;

    public AbstractArmor() {
        this.armor = armor;
    }

    public abstract int getArmor();
}
