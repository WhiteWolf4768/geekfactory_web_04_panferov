package ru.geekfactory.homefinance.dao.abstractFactoryExample;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CatFighter {
    private String name;
    private int hp;
    private AbstractCharacterClass characterClass;
    private AbstractArmor armor;
    private AbstractWeapon weapon;

    public CatFighter(String name, CatFighterAbstractFactory factory) {
        this.name = name;
        this.hp = 30;
        this.characterClass = factory.createClass();
        this.armor = factory.createArmor();
        this.weapon = factory.createWeapon();
    }

    public void attack(CatFighter target) {
        int damage = this.getWeapon().getDamage() - target.getArmor().getArmor();
        if (damage <= 0) {
            damage = 0;
        }
        target.setHp(target.getHp() - damage);
        System.out.println(this.getName() + " deal " + damage + " to target - " + target.getName());
        System.out.println("Target: " + target.getName() + " have " + target.getHp() + " HP");
    }

    public void getStatus() {
        System.out.print("Name:" + getName() + " |");
        System.out.print("Cat Class: " + getCharacterClass().getClassName() + " |");
        System.out.print("Cat HP: " + getHp() + " |");
        System.out.print("Car Armor: " + getArmor().getArmor() + " |");
        System.out.println("Cat Weapon damage: " + getWeapon().getDamage());
    }

    public boolean isDead() {
        if (this.hp <= 0) {
            System.out.println(this.getName() + " is dead!");
            return true;
        }
        return false;
    }
}
