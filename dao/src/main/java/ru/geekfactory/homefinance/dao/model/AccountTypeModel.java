package ru.geekfactory.homefinance.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountTypeModel {
    private Long id;
    private String type;

    public AccountTypeModel(String type) {
        this.type = type;
    }

}
