package ru.geekfactory.homefinance.dao.parsers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DOMParser implements Parser<TransactionModel> {
    @Override
    public List<TransactionModel> parseTransactions(String xmlFile) throws IOException, SAXException, ParserConfigurationException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(ClassLoader.getSystemResourceAsStream(xmlFile));

        List<TransactionModel> transactionModelList = new ArrayList<>();

        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {

            //We have encountered an <transactionModel> tag.
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                TransactionModel transactionModel = new TransactionModel();
                transactionModel.setId(Long.parseLong(node.getAttributes().getNamedItem("id").getNodeValue()));
                List<CategoryTransactionModel> categoryTransactionModelList;
                AccountModel accountModel = new AccountModel();

                NodeList childNodes = node.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node cNode = childNodes.item(j);

                    //Identifying the child tag of transactionModel encountered.
                    if (cNode instanceof Element) {
                        String content = cNode.getLastChild().getTextContent().trim();
                        switch (cNode.getNodeName()) {
                            case "amount":
                                transactionModel.setAmount(new BigDecimal(content));
                                break;
                            case "dateTime":
                                transactionModel.setDateTime(LocalDateTime.parse(content, formatter));
                                break;
                            case "accountModel":
                                accountModel.setId(Long.parseLong(cNode.getAttributes().getNamedItem("id").getNodeValue()));

                                NodeList cAccNodes = cNode.getChildNodes();
                                for (int l = 0; l < cAccNodes.getLength(); l++) {
                                    Node cAccNode = cAccNodes.item(l);
                                    if (cAccNode instanceof Element) {
                                        content = cAccNode.getLastChild().getTextContent().trim();
                                        switch (cAccNode.getNodeName()) {
                                            case "accountName":
                                                accountModel.setName(content);
                                                break;
                                            case "accountIsActive":
                                                accountModel.setActive(Boolean.parseBoolean(content));
                                                break;
                                            case "accountAmount":
                                                accountModel.setAmount(new BigDecimal(content));
                                                break;
                                        }
                                    }
                                }
                                break;

                            case "categories":
                                categoryTransactionModelList = new ArrayList<>();
                                NodeList cCatNodes = cNode.getChildNodes();
                                for (int k = 0; k < cCatNodes.getLength(); k++) {
                                    Node cCatNode = cCatNodes.item(k);
                                    if (cCatNode instanceof Element) {
                                        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
                                        categoryTransactionModel.setId(Long.parseLong(cCatNode.getAttributes().getNamedItem("id").getNodeValue()));

                                        NodeList catFields = cCatNode.getChildNodes();
                                        for (int l = 0; l < catFields.getLength(); l++) {
                                            Node catField = catFields.item(l);
                                            if (catField instanceof Element) {
                                                content = catField.getLastChild().getTextContent().trim();
                                                switch (catField.getNodeName()) {
                                                    case "categoryName":
                                                        categoryTransactionModel.setName(content);
                                                        categoryTransactionModelList.add(categoryTransactionModel);
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                                transactionModel.setCategoryTransactionModel(categoryTransactionModelList);
                                break;
                        }
                    }
                }
                transactionModel.setAccountModel(accountModel);
                transactionModelList.add(transactionModel);
            }
        }
        return transactionModelList;
    }
}
