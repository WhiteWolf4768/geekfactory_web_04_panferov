package ru.geekfactory.homefinance.dao.abstractFactoryExample.factories;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractArmor;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractCharacterClass;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractWeapon;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.CatFighterAbstractFactory;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.armors.LightArmor;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.classes.Rouge;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.weapons.Dagger;

public class AssassinFactory extends CatFighterAbstractFactory {

    @Override
    public AbstractCharacterClass createClass() {
        return new Rouge();
    }

    @Override
    public AbstractArmor createArmor() {
        return new LightArmor();
    }

    @Override
    public AbstractWeapon createWeapon() {
        return new Dagger();
    }
}
