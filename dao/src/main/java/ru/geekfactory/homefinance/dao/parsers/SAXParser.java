package ru.geekfactory.homefinance.dao.parsers;

import org.xml.sax.SAXException;
import ru.geekfactory.homefinance.dao.model.TransactionModel;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class SAXParser implements Parser<TransactionModel> {
    @Override
    public List<TransactionModel> parseTransactions(String xmlFile) throws IOException, SAXException, ParserConfigurationException {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        javax.xml.parsers.SAXParser parser = parserFactory.newSAXParser();
        SAXHandler handler = new SAXHandler();
        parser.parse(ClassLoader.getSystemResourceAsStream(xmlFile), handler);

        return handler.transactionModelList;
    }
}
