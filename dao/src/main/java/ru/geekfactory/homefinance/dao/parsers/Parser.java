package ru.geekfactory.homefinance.dao.parsers;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

public interface Parser<E> {
    List<E> parseTransactions(String xmlFile) throws IOException, SAXException, ParserConfigurationException, XMLStreamException;

    default void printEntryList(List<E> eList) {
        for (E e : eList) {
            System.out.println(e);
        }
    }
}
