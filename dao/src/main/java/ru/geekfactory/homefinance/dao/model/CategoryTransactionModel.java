package ru.geekfactory.homefinance.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryTransactionModel {
    private Long id;
    private String name;
    private CategoryTransactionModel parentCategoryTransactionModel;

    public CategoryTransactionModel(String name, CategoryTransactionModel parentCategoryTransactionModel) {
        this.name = name;
        this.parentCategoryTransactionModel = parentCategoryTransactionModel;
    }
}
