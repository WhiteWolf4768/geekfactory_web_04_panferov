package ru.geekfactory.homefinance.dao.abstractFactoryExample;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.factories.AssassinFactory;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.factories.HeavyKnightFactory;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.factories.KnightFactory;

public class Runner {
    public static void main(String[] args) {
        CatFighter cubeCat = new CatFighter("CubeCat", new HeavyKnightFactory());
        CatFighter neutralCat = new CatFighter("NeutralCat", new KnightFactory());
        CatFighter enemyCat = new CatFighter("Enemy", new AssassinFactory());

        System.out.println("CubeCat Status");
        cubeCat.getStatus();

        System.out.println("--------------");

        System.out.println("NeutralCat Status");
        neutralCat.getStatus();

        System.out.println("--------------");

        System.out.println("EnemyCat Status");
        enemyCat.getStatus();

        fight(cubeCat, enemyCat);
    }

    public static void fight(CatFighter cubeCat, CatFighter enemy) {
        System.out.println("FIGHT! FIGHT! FIGHT!");
        int round = 1;
        while (true) {
            System.out.println("------------------------------------------");
            System.out.println("ROUND - " + round);
            System.out.println("------------------------------------------");

            cubeCat.getStatus();
            enemy.getStatus();

            cubeCat.attack(enemy);
            if (enemy.isDead()) {
                System.out.println("Cat: " + cubeCat.getName() + " win the battle!!");
                break;
            }
            enemy.attack(cubeCat);
            if (cubeCat.isDead()) {
                System.out.println("Cat: " + cubeCat.getName() + " lose the battle!!");
                break;
            }

            round++;
        }
        System.out.println("Battle ends at round - " + round);
    }
}
