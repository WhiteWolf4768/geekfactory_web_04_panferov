package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.BankModel;

import java.sql.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class BankRepository implements Repository<BankModel> {
    private final static String INSERT = "INSERT INTO bankTbl (name) VALUES (?)";
    private final static String DELETE = "DELETE FROM bankTbl WHERE id = ?";
    private final static String SELECT_BY_ID = "SELECT id, name FROM bankTbl WHERE id = ?";
    private final static String SELECT_ALL = "SELECT id, name FROM bankTbl";
    private final static String UPDATE = "UPDATE bankTbl SET name = ? WHERE id = ?";
    private ConnectionSupplier connectionSupplier;

    public BankRepository() {
        this.connectionSupplier = new ConnectionSupplier();
    }

    public BankRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public Optional<BankModel> findById(Long id) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeQuery();

                BankModel model = null;

                ResultSet resultSet = preparedStatement.getResultSet();
                if (resultSet.next()) {
                    Long bankId = resultSet.getLong(1);
                    String name = resultSet.getString(2);

                    model = new BankModel(bankId, name);
                }

                return Optional.ofNullable(model);
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("BankModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("BankModel find error", e);
        }
    }

    @Override
    public Collection<BankModel> findAll() {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
                preparedStatement.executeQuery();
                HashSet<BankModel> bankModelHashSet = new HashSet<>();
                BankModel model;

                ResultSet resultSet = preparedStatement.getResultSet();
                while (resultSet.next()) {
                    Long bankId = resultSet.getLong(1);
                    String name = resultSet.getString(2);

                    model = new BankModel(bankId, name);
                    bankModelHashSet.add(model);
                }

                return bankModelHashSet;
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("BankModel find error", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("BankModel find error", e);
        }
    }

    @Override
    public BankModel insert(BankModel model) {
        try (Connection connection = connectionSupplier.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, model.getName());
                preparedStatement.executeUpdate();

                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }

                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("BankModel INSERT error" + model, e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("BankModel INSERT error" + model, e);
        }
    }

    @Override
    public void update(BankModel model) {
        if (model.getId() != null) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
                    preparedStatement.setString(1, model.getName());
                    preparedStatement.setLong(2, model.getId());
                    preparedStatement.executeUpdate();

                    connection.commit();
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("BankModel UPDATE error" + model, e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("BankModel UPDATE error" + model, e);
            }
        } else {
            System.out.println("Model id is NULL!");
        }
    }

    @Override
    public boolean delete(Long id) {
        if (findById(id).isPresent()) {
            try (Connection connection = connectionSupplier.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
                    preparedStatement.setLong(1, id);
                    preparedStatement.executeUpdate();

                    connection.commit();
                    return true;
                } catch (SQLException e) {
                    connection.rollback();
                    throw new HomeFinanceDaoException("BankModel DELETE error", e);
                }
            } catch (SQLException e) {
                throw new HomeFinanceDaoException("BankModel DELETE error", e);
            }
        } else {
            return false;
        }
    }
}
