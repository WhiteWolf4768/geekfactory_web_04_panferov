package ru.geekfactory.homefinance.dao.abstractFactoryExample.classes;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractCharacterClass;

public class Warrior extends AbstractCharacterClass {
    public Warrior() {
        className = "Warrior";
    }

    @Override
    public String getClassName() {
        return this.className;
    }
}
