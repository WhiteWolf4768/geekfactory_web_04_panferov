package ru.geekfactory.homefinance.dao.abstractFactoryExample;

public abstract class AbstractWeapon {
    protected int damage;

    public AbstractWeapon() {
        this.damage = damage;
    }

    public abstract int getDamage();
}
