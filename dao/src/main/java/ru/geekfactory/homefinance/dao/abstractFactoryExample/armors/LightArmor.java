package ru.geekfactory.homefinance.dao.abstractFactoryExample.armors;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractArmor;

public class LightArmor extends AbstractArmor {
    public LightArmor() {
        armor = 4;
    }

    @Override
    public int getArmor() {
        return this.armor;
    }
}
