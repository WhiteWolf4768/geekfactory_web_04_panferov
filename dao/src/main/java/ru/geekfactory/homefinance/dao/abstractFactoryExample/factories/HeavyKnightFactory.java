package ru.geekfactory.homefinance.dao.abstractFactoryExample.factories;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractArmor;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractCharacterClass;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractWeapon;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.CatFighterAbstractFactory;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.armors.HeavyArmor;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.classes.Warrior;
import ru.geekfactory.homefinance.dao.abstractFactoryExample.weapons.TwoHandedSword;

public class HeavyKnightFactory extends CatFighterAbstractFactory {
    @Override
    public AbstractCharacterClass createClass() {
        return new Warrior();
    }

    @Override
    public AbstractArmor createArmor() {
        return new HeavyArmor();
    }

    @Override
    public AbstractWeapon createWeapon() {
        return new TwoHandedSword();
    }
}
