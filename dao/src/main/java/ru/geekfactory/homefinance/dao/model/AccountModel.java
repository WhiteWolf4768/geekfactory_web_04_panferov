package ru.geekfactory.homefinance.dao.model;

import lombok.*;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountModel {
    private Long id;
    private String name;
    private CurrencyModel currencyModel;
    private AccountTypeModel accountTypeModel;
    private BankModel bankModel;
    private boolean isActive;
    private BigDecimal amount;

    public AccountModel(String name, CurrencyModel currency, AccountTypeModel accountTypeModel, BankModel bankModel, boolean isActive, BigDecimal amount) {
        this.name = name;
        this.currencyModel = currency;
        this.accountTypeModel = accountTypeModel;
        this.bankModel = bankModel;
        this.isActive = isActive;
        this.amount = amount;
    }
}
