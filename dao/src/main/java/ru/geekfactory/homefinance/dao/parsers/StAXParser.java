package ru.geekfactory.homefinance.dao.parsers;

import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class StAXParser implements Parser<TransactionModel> {
    @Override
    public List<TransactionModel> parseTransactions(String xmlFile) throws XMLStreamException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        List<TransactionModel> transactionModelList = null;
        List<CategoryTransactionModel> categoryTransactionModelList = null;
        TransactionModel currentTransactionModel = null;
        AccountModel currentAccountModel = null;
        CategoryTransactionModel categoryTransactionModel = null;
        String tagContent = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(ClassLoader.getSystemResourceAsStream(xmlFile));

        while (reader.hasNext()) {
            int event = reader.next();

            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if ("transactions".equals(reader.getLocalName())) {
                        transactionModelList = new ArrayList<>();
                    }
                    if ("transaction".equals(reader.getLocalName())) {
                        currentTransactionModel = new TransactionModel();
                        currentTransactionModel.setId(Long.parseLong(reader.getAttributeValue(0)));
                    }
                    if ("accountModel".equals(reader.getLocalName())) {
                        currentAccountModel = new AccountModel();
                        currentAccountModel.setId(Long.parseLong(reader.getAttributeValue(0)));
                    }
                    if ("categories".equals(reader.getLocalName())) {
                        categoryTransactionModelList = new ArrayList<>();
                    }
                    if ("category".equals(reader.getLocalName())) {
                        categoryTransactionModel = new CategoryTransactionModel();
                        categoryTransactionModel.setId(Long.parseLong(reader.getAttributeValue(0)));
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    tagContent = reader.getText().trim();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "amount":
                            currentTransactionModel.setAmount(new BigDecimal(tagContent));
                            break;
                        case "dateTime":
                            currentTransactionModel.setDateTime(LocalDateTime.parse(tagContent, formatter));
                            break;
                        case "accountName":
                            currentAccountModel.setName(tagContent);
                            break;
                        case "accountIsActive":
                            currentAccountModel.setActive(Boolean.parseBoolean(tagContent));
                            break;
                        case "accountAmount":
                            currentAccountModel.setAmount(new BigDecimal(tagContent));
                            break;
                        case "categoryName":
                            categoryTransactionModel.setName(tagContent);
                            categoryTransactionModelList.add(categoryTransactionModel);
                            break;
                        case "transaction":
                            currentTransactionModel.setAccountModel(currentAccountModel);
                            currentTransactionModel.setCategoryTransactionModel(categoryTransactionModelList);
                            transactionModelList.add(currentTransactionModel);
                            break;
                    }
                    break;

                case XMLStreamConstants.START_DOCUMENT:
                    transactionModelList = new ArrayList<>();
                    break;
            }
        }
        return transactionModelList;
    }
}
