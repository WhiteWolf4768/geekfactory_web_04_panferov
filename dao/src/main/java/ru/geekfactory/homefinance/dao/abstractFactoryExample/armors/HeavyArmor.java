package ru.geekfactory.homefinance.dao.abstractFactoryExample.armors;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractArmor;

public class HeavyArmor extends AbstractArmor {
    public HeavyArmor() {
        armor = 6;
    }

    @Override
    public int getArmor() {
        return this.armor;
    }
}
