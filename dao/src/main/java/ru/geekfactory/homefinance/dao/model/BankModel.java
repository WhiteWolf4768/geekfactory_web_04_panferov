package ru.geekfactory.homefinance.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BankModel {
    private Long id;
    private String name;

    public BankModel(String name) {
        this.name = name;
    }

}
