package ru.geekfactory.homefinance.dao.abstractFactoryExample.weapons;

import ru.geekfactory.homefinance.dao.abstractFactoryExample.AbstractWeapon;

public class TwoHandedSword extends AbstractWeapon {
    public TwoHandedSword() {
        damage = 12;
    }

    @Override
    public int getDamage() {
        return this.damage;
    }
}
