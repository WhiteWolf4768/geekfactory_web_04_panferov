package ru.geekfactory.homefinance.dao;

import com.sun.org.apache.bcel.internal.util.ClassPath;
import ru.geekfactory.homefinance.dao.model.*;
import ru.geekfactory.homefinance.dao.repository.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
//        testCurrencyRepository();
//        testBankRepository();
//        testCategoryTransactionRepository();
//        testAccountTypeRepository();
//        testAccountRepository();
//        testTransactionRepository();

    }

    public static void testCurrencyRepository() {
        CurrencyRepository currencyRepository = new CurrencyRepository();

//        CurrencyModel currencyModel = new CurrencyModel( "111", "111", "111");
        CurrencyModel currencyModel2 = new CurrencyModel(7l, "nya", "nya", "nya");
//        CurrencyModel currencyModel3 = new CurrencyModel( "333", "333", "333");
//
//        currencyModel = currencyRepository.insert(currencyModel);
//        System.out.println(currencyModel);
//
//        currencyModel2 = currencyRepository.insert(currencyModel2);
//        System.out.println(currencyModel2);
//
//        currencyModel3 = currencyRepository.insert(currencyModel3);
//        System.out.println(currencyModel3);

        System.out.println(currencyRepository.findById(1l).get());

        System.out.println(currencyRepository.delete(5l));

        currencyRepository.update(currencyModel2);

        System.out.println(currencyRepository.findAll().size());
        for (CurrencyModel setElement: currencyRepository.findAll()) {
            System.out.println(setElement);
        }
    }

    public static void testBankRepository() {
        BankRepository bankRepository = new BankRepository();

        BankModel bankModel1 = new BankModel("SberBank");
        BankModel bankModel2 = new BankModel("AlphaBank");
        BankModel bankModel3 = new BankModel(1l,"NyaaaaaaaaaaCATBank");

//        bankModel1 = bankRepository.insert(bankModel1);
//        System.out.println(bankModel1);
//        bankModel2 = bankRepository.insert(bankModel2);
//        System.out.println(bankModel2);
//        bankModel3 = bankRepository.insert(bankModel3);
//        System.out.println(bankModel3);

        System.out.println(bankRepository.findById(3l));

        System.out.println(bankRepository.delete(3l));

        bankRepository.update(bankModel3);

        System.out.println(bankRepository.findAll().size());
        for (BankModel setElement: bankRepository.findAll()) {
            System.out.println(setElement);
        }
    }

    public static void testCategoryTransactionRepository() {
        CategoryTransactionRepository categoryTransactionRepository = new CategoryTransactionRepository();

        System.out.println(categoryTransactionRepository.findById(1l));
        System.out.println(categoryTransactionRepository.findById(2l));
        System.out.println(categoryTransactionRepository.findById(3l));
        System.out.println(categoryTransactionRepository.findById(4l));

        CategoryTransactionModel categoryTransactionModel1 = new CategoryTransactionModel("TEST333333333333333", null);
        CategoryTransactionModel categoryTransactionModel2 =
                new CategoryTransactionModel("TEST4444444444444", new CategoryTransactionModel(5l, "test", null));

        System.out.println(categoryTransactionRepository.insert(categoryTransactionModel1));
        System.out.println(categoryTransactionRepository.insert(categoryTransactionModel2));
        System.out.println();

        categoryTransactionRepository.update(
                new CategoryTransactionModel(7l, "UPDATED", new CategoryTransactionModel(2l, "test", null)));

        System.out.println(categoryTransactionRepository.findAll().size());
        for (CategoryTransactionModel setElement: categoryTransactionRepository.findAll()) {
            System.out.println(setElement);
        }
    }

    public static void testAccountTypeRepository() {
        AccountTypeRepository accountTypeRepository = new AccountTypeRepository();

        AccountTypeModel accountTypeModel1 = new AccountTypeModel("Card");
        AccountTypeModel accountTypeModel2 = new AccountTypeModel("Cash");
        AccountTypeModel accountTypeModel3 = new AccountTypeModel("Debit");
        AccountTypeModel accountTypeModel4 = new AccountTypeModel(1l, "UPDATE");

        System.out.println(accountTypeRepository.insert(accountTypeModel1));
        System.out.println(accountTypeRepository.insert(accountTypeModel2));
        System.out.println(accountTypeRepository.insert(accountTypeModel3));

        System.out.println(accountTypeRepository.findById(3l));

        System.out.println(accountTypeRepository.delete(3l));

        accountTypeRepository.update(accountTypeModel4);

        System.out.println(accountTypeRepository.findAll().size());
        for (AccountTypeModel setElement: accountTypeRepository.findAll()) {
            System.out.println(setElement);
        }
    }

    public static void testAccountRepository() {
        AccountRepository accountRepository = new AccountRepository();

        AccountModel accountModel1 = new AccountModel("acc3333", new CurrencyModel(1l, null, null, null),
                new AccountTypeModel(1l, null), new BankModel(1l, null), true, BigDecimal.valueOf(2000l));

        AccountModel accountModel2 = new AccountModel("acc44444444", new CurrencyModel(2l, null, null, null),
                new AccountTypeModel(2l, null), new BankModel(2l, null), true, BigDecimal.valueOf(40000l));

        AccountModel accountModel3 = new AccountModel(3l,"UpdatedAccount3333333", new CurrencyModel(2l, null, null, null),
                new AccountTypeModel(2l, null), new BankModel(3l, null), false, BigDecimal.valueOf(10l));

//        System.out.println(accountRepository.insert(accountModel1));
//        System.out.println(accountRepository.insert(accountModel2));


        System.out.println(accountRepository.findById(3l));

        System.out.println(accountRepository.delete(4l));

        accountRepository.update(accountModel3);

        System.out.println(accountRepository.findAll().size());
        for (AccountModel setElement: accountRepository.findAll()) {
            System.out.println(setElement);
        }
    }

    public static void testTransactionRepository() {
        TransactionRepository transactionRepository = new TransactionRepository();

//        HashSet<CategoryTransactionModel> hashSet = new HashSet<>();
//        hashSet.add(new CategoryTransactionModel(1l));
//        hashSet.add(new CategoryTransactionModel(2l));

//        HashSet<CategoryTransactionModel> hashSet2 = new HashSet<>();
//        hashSet2.add(new CategoryTransactionModel(3l, "test", null));
//        hashSet2.add(new CategoryTransactionModel(4l, "nya", null));
//
//        TransactionModel transactionModel = new TransactionModel(BigDecimal.valueOf(3000l), LocalDateTime.now(), new AccountModel(1l), hashSet);
//        TransactionModel transactionModel2 = new TransactionModel( BigDecimal.valueOf(123456l), LocalDateTime.now(),
//                new AccountModel(2l, "test", null, null, null, true, null), hashSet2);

        System.out.println("nya");
//        System.out.println(transactionRepository.insert(transactionModel2));
        System.out.println(transactionRepository.findById(6l));
        System.out.println();
//        transactionRepository.update(transactionModel2);
        System.out.println(transactionRepository.findAll().size());
        for (TransactionModel setElement: transactionRepository.findAll()) {
            System.out.println(setElement);
        }


    }
}
