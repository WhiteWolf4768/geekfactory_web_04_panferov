package ru.geekfactory.homefinance.dao.parsers;

import org.xml.sax.SAXException;
import ru.geekfactory.homefinance.dao.model.TransactionModel;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

public class Starter {
    public static void main(String[] args) throws SAXException, XMLStreamException, ParserConfigurationException, IOException {
        Parser<TransactionModel> domParser = new DOMParser();
        List<TransactionModel> transactionListDOM = domParser.parseTransactions("transaction.xml");
        domParser.printEntryList(transactionListDOM);

        System.out.println("--------------------------------------------");

        Parser<TransactionModel> saxParser = new SAXParser();
        List<TransactionModel> transactionListSAX = saxParser.parseTransactions("transaction.xml");
        saxParser.printEntryList(transactionListSAX);

        System.out.println("--------------------------------------------");

        Parser<TransactionModel> staxParser = new StAXParser();
        List<TransactionModel> transactionListStAX = staxParser.parseTransactions("transaction.xml");
        staxParser.printEntryList(transactionListStAX);

    }
}
