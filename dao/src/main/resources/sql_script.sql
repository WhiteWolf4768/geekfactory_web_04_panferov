DROP TABLE IF EXISTS transactionCategoryTbl;
DROP TABLE IF EXISTS transactionTbl;
DROP TABLE IF EXISTS categoryTbl;
DROP TABLE IF EXISTS accountTypeTbl;
DROP TABLE IF EXISTS currencyTbl;
DROP TABLE IF EXISTS bankTbl;
DROP TABLE IF EXISTS accountTbl;

create table currencyTbl (
    id int auto_increment primary key not null,
    name varchar(30) unique not null,
    code varchar(30) not null,
    symbol varchar(30) not null
);

create table categoryTbl (
    id int auto_increment primary key not null,
    name varchar(30) unique not null,

    parentCategoryId int,
    foreign key (parentCategoryId) references categoryTbl (id)
);

create table bankTbl (
    id int auto_increment primary key not null,
    name varchar(30) unique not null
);

create table accountTypeTbl (
    id int auto_increment primary key not null,
    name varchar(30) unique not null
);

create table accountTbl (
    id int auto_increment primary key not null,
    name varchar(30) unique not null,
    isActive boolean not null,
    amount decimal(15, 2) not null,

    currencyId int,
    foreign key (currencyId) references currencyTbl (id),

    accountTypeId int,
    foreign key (accountTypeId) references accountTypeTbl (id),

    bankId int,
    foreign key (bankId) references bankTbl (id)
);

create table transactionTbl (
    id int auto_increment primary key not null,
    amount decimal(15, 2) not null,
    dateTime datetime not null,

    accountId int,
    foreign key (accountId) references accountTbl (id)
);

create table transactionCategoryTbl(
    id int auto_increment primary key not null,
    transactionId int not null,
    foreign key (transactionId) references transactionTbl (id) on delete cascade,
    categoryId int not null,
    foreign key (categoryId) references categoryTbl (id)
);

