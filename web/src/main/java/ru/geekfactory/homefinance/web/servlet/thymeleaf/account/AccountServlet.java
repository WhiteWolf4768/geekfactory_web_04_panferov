package ru.geekfactory.homefinance.web.servlet.thymeleaf.account;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.*;
import ru.geekfactory.homefinance.service.services.AccountService;
import ru.geekfactory.homefinance.service.services.AccountTypeService;
import ru.geekfactory.homefinance.service.services.BankService;
import ru.geekfactory.homefinance.service.services.CurrencyService;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.CustomHttpServlet;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.config.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;

@WebServlet("/account")
public class AccountServlet extends CustomHttpServlet {
    private final AccountService accountService = new AccountService();
    private final AccountTypeService accountTypeService = new AccountTypeService();
    private final BankService bankService = new BankService();
    private final CurrencyService currencyService = new CurrencyService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        Collection<AccountTypeModel> accountTypeModels = accountTypeService.findAll();
        Collection<BankModel> bankModels = bankService.findAll();
        Collection<CurrencyModel> currencyModels = currencyService.findAll();


        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            AccountModel accountModel = accountService.findById(Long.parseLong(req.getParameter("id"))).orElse(null);

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("account", accountModel);
            context.setVariable("accountTypeModels", accountTypeModels);
            context.setVariable("bankModels", bankModels);
            context.setVariable("currencyModels", currencyModels);
            templateEngine.process("account/accountFind", context, resp.getWriter());
        } else {
            Collection<AccountModel> accountModels = accountService.findAll();

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("accountModels", accountModels);
            context.setVariable("accountTypeModels", accountTypeModels);
            context.setVariable("bankModels", bankModels);
            context.setVariable("currencyModels", currencyModels);
            templateEngine.process("account/accountList", context, resp.getWriter());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        AccountModel accountModel = prepareModel(req);
        Long accountId = null;
        try {
            accountId = accountService.insert(accountModel).getId();
            context.setVariable("accountId", accountId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "create");
            templateEngine.process("account/accountAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("accountId", accountId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "create");
            templateEngine.process("account/accountAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        Long accountId = Long.parseLong(req.getParameter("id"));

        AccountModel accountModel = prepareModel(req);
        accountModel.setId(accountId);
        try {
            accountService.update(accountModel);
            context.setVariable("accountId", accountId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "update");
            templateEngine.process("account/accountAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("accountId", accountId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "update");
            templateEngine.process("account/accountAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            Long id = Long.parseLong(req.getParameter("id"));
            try {
                accountService.delete(id);
                context.setVariable("accountId", id);
                context.setVariable("exception", false);
                context.setVariable("alertType", "delete");
                templateEngine.process("account/accountAlert", context, resp.getWriter());
            } catch (HomeFinanceDaoException e) {
                context.setVariable("accountId", id);
                context.setVariable("exception", true);
                context.setVariable("alertType", "delete");
                templateEngine.process("account/accountAlert", context, resp.getWriter());
            }
        }
    }

    private AccountModel prepareModel(HttpServletRequest req) {
        String name = req.getParameter("accountName");
        boolean isActive = Boolean.parseBoolean(req.getParameter("isActive"));
        BigDecimal amount = new BigDecimal(req.getParameter("amount"));
        Long accountTypeId = Long.parseLong(req.getParameter("accountType"));
        Long bankId = Long.parseLong(req.getParameter("bank"));
        Long currencyId = Long.parseLong(req.getParameter("currency"));

        AccountTypeModel accountTypeModel = new AccountTypeModel(accountTypeId, null);
        BankModel bankModel = new BankModel(bankId, null);
        CurrencyModel currencyModel = new CurrencyModel(currencyId, null, null, null);

        AccountModel accountModel = new AccountModel(name, currencyModel, accountTypeModel, bankModel, isActive, amount);

        return accountModel;
    }

}
