package ru.geekfactory.homefinance.web.servlet.thymeleaf.currency;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.service.services.CurrencyService;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.CustomHttpServlet;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.config.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/currency")
public class CurrencyServlet extends CustomHttpServlet {
    private final CurrencyService currencyService = new CurrencyService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            CurrencyModel currencyModel = currencyService.findById(Long.parseLong(req.getParameter("id"))).orElse(null);

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("currency", currencyModel);
            templateEngine.process("currency/currencyFind", context, resp.getWriter());
        } else {
            Collection<CurrencyModel> currencyModels = currencyService.findAll();

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("currencyModels", currencyModels);
            templateEngine.process("currency/currencyList", context, resp.getWriter());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        String currencyName = req.getParameter("currencyName");
        String code = req.getParameter("code");
        String symbol = req.getParameter("symbol");

        CurrencyModel currencyModel = new CurrencyModel(currencyName, code, symbol);
        Long currencyId = null;
        try {
            currencyId = currencyService.insert(currencyModel).getId();
            context.setVariable("currencyId", currencyId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "create");
            templateEngine.process("currency/currencyAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("currencyId", currencyId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "create");
            templateEngine.process("currency/currencyAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        Long currencyId = Long.parseLong(req.getParameter("id"));
        String currencyName = req.getParameter("currencyName");
        String code = req.getParameter("code");
        String symbol = req.getParameter("symbol");

        CurrencyModel currencyModel = new CurrencyModel(currencyId, currencyName, code, symbol);

        try {
            currencyService.update(currencyModel);
            context.setVariable("currencyId", currencyId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "update");
            templateEngine.process("currency/currencyAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("currencyId", currencyId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "update");
            templateEngine.process("currency/currencyAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            Long id = Long.parseLong(req.getParameter("id"));
            try {
                currencyService.delete(id);
                context.setVariable("currencyId", id);
                context.setVariable("exception", false);
                context.setVariable("alertType", "delete");
                templateEngine.process("currency/currencyAlert", context, resp.getWriter());
            } catch (HomeFinanceDaoException e) {
                context.setVariable("currencyId", id);
                context.setVariable("exception", true);
                context.setVariable("alertType", "delete");
                templateEngine.process("currency/currencyAlert", context, resp.getWriter());
            }
        }
    }
}
