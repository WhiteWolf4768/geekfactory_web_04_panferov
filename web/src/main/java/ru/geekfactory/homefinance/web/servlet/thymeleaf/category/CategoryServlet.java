package ru.geekfactory.homefinance.web.servlet.thymeleaf.category;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.service.services.CategoryTransactionService;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.CustomHttpServlet;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.config.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/category")
public class CategoryServlet extends CustomHttpServlet {
    private final CategoryTransactionService categoryTransactionService = new CategoryTransactionService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        Collection<CategoryTransactionModel> categoryTransactionList = categoryTransactionService.findAll();

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            CategoryTransactionModel categoryTransactionModel = categoryTransactionService.findById(Long.parseLong(req.getParameter("id"))).orElse(null);

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("category", categoryTransactionModel);
            context.setVariable("categoryList", categoryTransactionList);
            templateEngine.process("category/categoryFind", context, resp.getWriter());
        } else {
            Collection<CategoryTransactionModel> categoryTransactionModels = categoryTransactionService.findAll();

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("categoryModels", categoryTransactionModels);
            context.setVariable("categoryList", categoryTransactionList);
            templateEngine.process("category/categoryList", context, resp.getWriter());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        CategoryTransactionModel categoryTransactionModel = prepareModel(req);
        Long categoryId = null;
        try {
            categoryId = categoryTransactionService.insert(categoryTransactionModel).getId();
            context.setVariable("categoryId", categoryId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "create");
            templateEngine.process("category/categoryAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("categoryId", categoryId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "create");
            templateEngine.process("category/categoryAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        Long categoryId = Long.parseLong(req.getParameter("id"));

        CategoryTransactionModel categoryTransactionModel = prepareModel(req);
        categoryTransactionModel.setId(categoryId);

        try {
            categoryTransactionService.update(categoryTransactionModel);
            context.setVariable("categoryId", categoryId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "update");
            templateEngine.process("category/categoryAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("categoryId", categoryId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "update");
            templateEngine.process("category/categoryAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            Long id = Long.parseLong(req.getParameter("id"));
            try {
                categoryTransactionService.delete(id);
                context.setVariable("categoryId", id);
                context.setVariable("exception", false);
                context.setVariable("alertType", "delete");
                templateEngine.process("category/categoryAlert", context, resp.getWriter());
            } catch (HomeFinanceDaoException e) {
                context.setVariable("categoryId", id);
                context.setVariable("exception", true);
                context.setVariable("alertType", "delete");
                templateEngine.process("category/categoryAlert", context, resp.getWriter());
            }
        }
    }

    private CategoryTransactionModel prepareModel(HttpServletRequest req) {
        String categoryName = req.getParameter("categoryName");
        Long parentCategoryId;
        if ((req.getParameter("parentCategoryId") != null) && (!req.getParameter("parentCategoryId").equals(""))) {
            parentCategoryId = Long.parseLong(req.getParameter("parentCategoryId"));
        } else {
            parentCategoryId = null;
        }
        CategoryTransactionModel parentCategory = new CategoryTransactionModel(parentCategoryId, null, null);

        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(categoryName, parentCategory);

        return categoryTransactionModel;
    }
}
