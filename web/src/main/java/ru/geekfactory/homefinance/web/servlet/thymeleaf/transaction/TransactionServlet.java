package ru.geekfactory.homefinance.web.servlet.thymeleaf.transaction;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.AccountModel;
import ru.geekfactory.homefinance.dao.model.CategoryTransactionModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.service.services.AccountService;
import ru.geekfactory.homefinance.service.services.CategoryTransactionService;
import ru.geekfactory.homefinance.service.services.TransactionService;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.CustomHttpServlet;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.config.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashSet;

@WebServlet("/transaction")
public class TransactionServlet extends CustomHttpServlet {
    private final TransactionService transactionService = new TransactionService();
    private final AccountService accountService = new AccountService();
    private final CategoryTransactionService categoryTransactionService = new CategoryTransactionService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());
        Collection<AccountModel> accountModels = accountService.findAll();
        Collection<CategoryTransactionModel> categoryTransactionModels = categoryTransactionService.findAll();

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            TransactionModel transactionModel = transactionService.findById(Long.parseLong(req.getParameter("id"))).orElse(null);

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("transactionModel", transactionModel);
            context.setVariable("accountModels", accountModels);
            context.setVariable("categoryModels", categoryTransactionModels);
            templateEngine.process("transaction/transactionFind", context, resp.getWriter());
        } else {
            Collection<TransactionModel> transactionModels = transactionService.findAll();

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("transactionModels", transactionModels);
            context.setVariable("accountModels", accountModels);
            context.setVariable("categoryModels", categoryTransactionModels);
            templateEngine.process("transaction/transactionList", context, resp.getWriter());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        TransactionModel transactionModel = prepareModel(req);
        Long transactionId = null;
        try {
            transactionId = transactionService.insert(transactionModel).getId();
            context.setVariable("transactionId", transactionId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "create");
            templateEngine.process("transaction/transactionAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("transactionId", transactionId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "create");
            templateEngine.process("transaction/transactionAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        Long transactionId = Long.parseLong(req.getParameter("id"));

        TransactionModel transactionModel = prepareModel(req);
        transactionModel.setId(transactionId);
        try {
            transactionService.update(transactionModel);
            context.setVariable("transactionId", transactionId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "update");
            templateEngine.process("transaction/transactionAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("transactionId", transactionId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "update");
            templateEngine.process("transaction/transactionAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            Long id = Long.parseLong(req.getParameter("id"));
            try {
                transactionService.delete(id);
                context.setVariable("transactionId", id);
                context.setVariable("exception", false);
                context.setVariable("alertType", "delete");
                templateEngine.process("transaction/transactionAlert", context, resp.getWriter());
            } catch (HomeFinanceDaoException e) {
                context.setVariable("transactionId", id);
                context.setVariable("exception", true);
                context.setVariable("alertType", "delete");
                templateEngine.process("transaction/transactionAlert", context, resp.getWriter());
            }
        }
    }

    private TransactionModel prepareModel(HttpServletRequest req) {
        BigDecimal amount = new BigDecimal(req.getParameter("amount"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse((req.getParameter("date") + " " + req.getParameter("time")), formatter);
        Long accountId = Long.parseLong(req.getParameter("account"));
        AccountModel accountModel = new AccountModel(accountId, "name", null, null, null, true, null);
        Collection<CategoryTransactionModel> categoryTransactionModels = new HashSet<>();

        String[] values = req.getParameterValues("category");
        for (int i = 0; i < values.length; i++) {
            Long id = Long.parseLong(values[i]);
            CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(id, null, null);
            categoryTransactionModels.add(categoryTransactionModel);
        }

        TransactionModel transactionModel = new TransactionModel(amount, dateTime, accountModel, categoryTransactionModels);

        return transactionModel;
    }
}
