package ru.geekfactory.homefinance.web.servlet.thymeleaf.bank;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.service.services.BankService;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.CustomHttpServlet;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.config.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/bank")
public class BankServlet extends CustomHttpServlet {
    private final BankService bankService = new BankService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            BankModel bankModel = bankService.findById(Long.parseLong(req.getParameter("id"))).orElse(null);

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("bank", bankModel);
            templateEngine.process("bank/bankFind", context, resp.getWriter());
        } else {
            Collection<BankModel> bankModels = bankService.findAll();

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("bankModels", bankModels);
            templateEngine.process("bank/bankList", context, resp.getWriter());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        String bankName = req.getParameter("bankName");

        BankModel bankModel = new BankModel(bankName);
        Long bankId = null;
        try {
            bankId = bankService.insert(bankModel).getId();
            context.setVariable("bankId", bankId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "create");
            templateEngine.process("bank/bankAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("bankId", bankId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "create");
            templateEngine.process("bank/bankAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        Long bankId = Long.parseLong(req.getParameter("id"));
        String bankName = req.getParameter("bankName");

        BankModel bankModel = new BankModel(bankId, bankName);

        try {
            bankService.update(bankModel);
            context.setVariable("bankId", bankId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "update");
            templateEngine.process("bank/bankAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("bankId", bankId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "update");
            templateEngine.process("bank/bankAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            Long id = Long.parseLong(req.getParameter("id"));
            try {
                bankService.delete(id);
                context.setVariable("bankId", id);
                context.setVariable("exception", false);
                context.setVariable("alertType", "delete");
                templateEngine.process("bank/bankAlert", context, resp.getWriter());
            } catch (HomeFinanceDaoException e) {
                context.setVariable("bankId", id);
                context.setVariable("exception", true);
                context.setVariable("alertType", "delete");
                templateEngine.process("bank/bankAlert", context, resp.getWriter());
            }
        }
    }
}
