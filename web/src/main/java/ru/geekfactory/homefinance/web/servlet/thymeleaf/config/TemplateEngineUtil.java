package ru.geekfactory.homefinance.web.servlet.thymeleaf.config;

import org.thymeleaf.TemplateEngine;

import javax.servlet.ServletContext;

public class TemplateEngineUtil {
    private static final String TEMPLATE_ENGINE_ATTRIBUTE = "TemplateEngineInstance";

    public static void storeTemplateEngine(ServletContext context, TemplateEngine engine) {
        context.setAttribute(TEMPLATE_ENGINE_ATTRIBUTE, engine);
    }

    public static TemplateEngine getTemplateEngine(ServletContext context) {
        return (TemplateEngine) context.getAttribute(TEMPLATE_ENGINE_ATTRIBUTE);
    }
}
