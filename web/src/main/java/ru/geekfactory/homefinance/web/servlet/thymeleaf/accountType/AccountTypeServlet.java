package ru.geekfactory.homefinance.web.servlet.thymeleaf.accountType;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import ru.geekfactory.homefinance.dao.exceptions.HomeFinanceDaoException;
import ru.geekfactory.homefinance.dao.model.AccountTypeModel;
import ru.geekfactory.homefinance.dao.model.BankModel;
import ru.geekfactory.homefinance.service.services.AccountTypeService;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.CustomHttpServlet;
import ru.geekfactory.homefinance.web.servlet.thymeleaf.config.TemplateEngineUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/accountType")
public class AccountTypeServlet extends CustomHttpServlet {
    private final AccountTypeService accountTypeService = new AccountTypeService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            AccountTypeModel accountTypeModel = accountTypeService.findById(Long.parseLong(req.getParameter("id"))).orElse(null);

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("accountType", accountTypeModel);
            templateEngine.process("accountType/accountTypeFind", context, resp.getWriter());
        } else {
            Collection<AccountTypeModel> accountTypeModels = accountTypeService.findAll();

            WebContext context = new WebContext(req, resp, req.getServletContext());
            context.setVariable("accountTypeModels", accountTypeModels);
            templateEngine.process("accountType/accountTypeList", context, resp.getWriter());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        String typeName = req.getParameter("typeName");

        AccountTypeModel accountTypeModel = new AccountTypeModel(typeName);
        Long accTypeId = null;
        try {
            accTypeId = accountTypeService.insert(accountTypeModel).getId();
            context.setVariable("accTypeId", accTypeId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "create");
            templateEngine.process("accountType/accountTypeAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("accTypeId", accTypeId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "create");
            templateEngine.process("accountType/accountTypeAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        Long accTypeId = Long.parseLong(req.getParameter("id"));
        String typeName = req.getParameter("typeName");

        AccountTypeModel accountTypeModel = new AccountTypeModel(accTypeId, typeName);

        try {
            accountTypeService.update(accountTypeModel);
            context.setVariable("accTypeId", accTypeId);
            context.setVariable("exception", false);
            context.setVariable("alertType", "update");
            templateEngine.process("accountType/accountTypeAlert", context, resp.getWriter());
        } catch (HomeFinanceDaoException e) {
            context.setVariable("accTypeId", accTypeId);
            context.setVariable("exception", true);
            context.setVariable("alertType", "update");
            templateEngine.process("accountType/accountTypeAlert", context, resp.getWriter());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TemplateEngine templateEngine = TemplateEngineUtil.getTemplateEngine(req.getServletContext());

        WebContext context = new WebContext(req, resp, req.getServletContext());

        if ((req.getParameter("id") != null) && !req.getParameter("id").equals("")) {
            Long id = Long.parseLong(req.getParameter("id"));
            try {
                accountTypeService.delete(id);
                context.setVariable("accTypeId", id);
                context.setVariable("exception", false);
                context.setVariable("alertType", "delete");
                templateEngine.process("accountType/accountTypeAlert", context, resp.getWriter());
            } catch (HomeFinanceDaoException e) {
                context.setVariable("accTypeId", id);
                context.setVariable("exception", true);
                context.setVariable("alertType", "delete");
                templateEngine.process("accountType/accountTypeAlert", context, resp.getWriter());
            }
        }
    }
}
